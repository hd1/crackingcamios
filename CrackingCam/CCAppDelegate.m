//
//  CCAppDelegate.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Parse/Parse.h>
#import <GoogleMaps/GoogleMaps.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <CoreLocation/CoreLocation.h>

#import "CCAppDelegate.h"
#import "CCLoginViewController.h"
#import "CCHomeViewController.h"
#import "CCGlobalData.h"
#import "CCFriendsListViewController.h"

@interface CCAppDelegate ()
{
    CLLocationManager *locationManager;
}
@end

@implementation CCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Parse setApplicationId:@"XE4HtlexcJc1tKaAjrmEcdY4h2NwvrpJDudclurQ"
                  clientKey:@"d8uYFd81ISI2v3URrJijkMI5BuKHaW5amCnPamHr"];
    [FBLoginView class];
    [PFFacebookUtils initializeFacebook];
    [GMSServices provideAPIKey:@"AIzaSyAPqVDIty0AC1Y0wQ-9ZH7NU1nipV3iioY"];
    
    [CCGlobalData sharedGlobalData].fbFriendsIds = [[NSMutableArray alloc]init];
    [CCGlobalData sharedGlobalData].friends = [[NSMutableArray alloc]init];
        
    [SDImageCache sharedImageCache].maxCacheAge = 24*60*60;
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    [self processNotificationData:notificationPayload];
    locationManager = [[CLLocationManager alloc]init];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:[PFFacebookUtils session]];

}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [CCGlobalData sharedGlobalData].deviceToken = [[[[deviceToken description]
                          stringByReplacingOccurrencesOfString:@"<"withString:@""]
                         stringByReplacingOccurrencesOfString:@">" withString:@""]
                        stringByReplacingOccurrencesOfString: @" " withString: @""];
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

- (void)processNotificationData:(NSDictionary *)notificationPayload
{
    if ([notificationPayload objectForKey:@"NewFriend"] && [PFUser currentUser]) {
        [CCGlobalData sharedGlobalData].fbFriendObjectId = [notificationPayload objectForKey:@"NewFriend"];
    }
    else if ([notificationPayload objectForKey:@"locationId"] && [PFUser currentUser]) {
        [CCGlobalData sharedGlobalData].remoteNotificationLocationId = [notificationPayload objectForKey:@"locationId"];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    if ([PFUser currentUser] && locationManager.location) {
        PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLocation:locationManager.location];
        [[PFUser currentUser]setObject:geoPoint forKey:@"lastLocation"];
    }
}

@end
