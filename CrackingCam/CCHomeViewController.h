//
//  CCHomeViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "SWRevealViewController.h"
#import "CCParentViewController.h"

@class CCGooglePlacesPlacesSearchQuery;

@interface CCHomeViewController : CCParentViewController<GMSMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate, SWRevealViewControllerDelegate,CLLocationManagerDelegate>
{
    NSArray *searchResultPlaces;
    CCGooglePlacesPlacesSearchQuery *placesSearchQuery;
}

@property (strong, nonatomic) GMSMapView *mapView;
@property BOOL shouldShowGetStartedTitle;

@end
