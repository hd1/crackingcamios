//
//  CCSideMenuViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/13/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCSideMenuViewController.h"
#import <Parse/Parse.h>
#import "CCLoginViewController.h"
#import "SWRevealViewController.h"
#import "CCGlobalData.h"
@interface CCSideMenuViewController ()
{
    SWRevealViewController *revealVC;
}
@end

@implementation CCSideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.scrollEnabled   = NO;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]]];
    [self.tableView setBackgroundView:nil];
    if ([CCGlobalData sharedGlobalData].fbFriendObjectId) {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logoutUser) name:@"logoutUser" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Home";
            cell.imageView.image = [UIImage imageNamed:@"Home.png"];
            break;
        case 1:
            cell.textLabel.text = @"Queue";
            cell.imageView.image = [UIImage imageNamed:@"Queue.png"];
            break;
        case 2:
            cell.textLabel.text = @"Friends";
            cell.imageView.image = [UIImage imageNamed:@"Friends.png"];
            break;
        case 3:
            cell.textLabel.text = @"About Us";
            cell.imageView.image = [UIImage imageNamed:@"About-Us.png"];
            break;
        case 4:
            cell.textLabel.text = @"Contact Us";
            cell.imageView.image = [UIImage imageNamed:@"Contact.png"];
            break;
        case 5:
            cell.textLabel.text = @"Help";
            cell.imageView.image = [UIImage imageNamed:@"Help.png"];
            break;
        case 6:
            cell.textLabel.text = @"Logout";
            cell.imageView.image = [UIImage imageNamed:@"Logout.png"];
            break;
        default:
            break;
    }
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"home" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"queue" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"friendsList" sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:@"aboutUs" sender:self];
            break;
        case 4:
            [self performSegueWithIdentifier:@"contactUs" sender:self];
            break;
        case 5:
            [self performSegueWithIdentifier:@"toHelp" sender:self];
            break;
        case 6:
            [PFUser logOut];
            [self performSegueWithIdentifier:@"login" sender:self];
            break;
            
        default:
            break;
    }
}

- (void)logoutUser
{
    [PFUser logOut];
    [self performSegueWithIdentifier:@"login" sender:self];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    
}


@end
