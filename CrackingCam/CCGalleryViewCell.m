//
//  CCGalleryViewCell.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/18/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCGalleryViewCell.h"
#import "CCGalleryViewController.h"
@implementation CCGalleryViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      
        self.userBG = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, 44.0)];
        [self.userBG setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Sticky-Footer-BG.png"]]];
        
        self.userProfilePic = [[UIImageView alloc]initWithFrame:CGRectMake(5.0, 5.0, 36.0, 36.0)];
        self.userProfilePic.layer.cornerRadius = 5.0;
        self.userProfilePic.layer.masksToBounds= YES;
        self.userProfilePic.layer.borderWidth   = 1.0;
        self.userProfilePic.layer.borderColor   = [UIColor whiteColor].CGColor;
        [self.userBG addSubview:self.userProfilePic];
        
        self.userName = [[UILabel alloc]initWithFrame:CGRectMake(46.0, 11.0, 250.0, 21.0)];
        self.userName.textColor = [UIColor whiteColor];
        self.userName.font      = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        [self.userBG addSubview:self.userName];

        self.closeButton = [[UIButton alloc]initWithFrame:CGRectMake(self.contentView.frame.size.width - 44.0, 0.0, 44.0, 44.0)];
        [self.closeButton setImage:[UIImage imageNamed:@"Close_Gallery.png"] forState:UIControlStateNormal];
        [self.userBG addSubview:self.closeButton];
        
        self.imageView = [[PFImageView alloc]initWithFrame:CGRectMake(0.0, 89.0, self.contentView.frame.size.width, [UIScreen mainScreen].bounds.size.height- 89.0)];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.ratingImage = [[UIImageView alloc]initWithFrame:CGRectMake(10.0, self.imageView.frame.size.height - 70.0, 30.0, 30.0)];
        [self.imageView addSubview:self.ratingImage];
        
        self.ratingLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.ratingImage.frame.origin.x + self.ratingImage.frame.size.width + 5.0,  self.imageView.frame.size.height - 70.0, 100, 30.0)];
        self.ratingLabel.textColor = [UIColor whiteColor];
        self.ratingLabel.font      = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0];
        [self.imageView addSubview:self.ratingLabel];
        
        self.captionLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0,self.imageView.frame.size.height/2.0 , 320.0, 36.0)];
        self.captionLabel.textColor = [UIColor whiteColor];
        self.captionLabel.font      = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        self.captionLabel.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        self.captionLabel.adjustsFontSizeToFitWidth = YES;
        self.captionLabel.textAlignment = NSTextAlignmentCenter;
        [self.imageView addSubview:self.captionLabel];
        
        self.time = [[UILabel alloc]initWithFrame:CGRectMake(13.0, self.ratingLabel.frame.origin.y + self.ratingLabel.frame.size.height + 3.0, 100.0, 18.0)];
        self.time.textColor = [UIColor whiteColor];
        self.time.font      = [UIFont fontWithName:@"HelveticaNeue" size:10.0];
        [self.imageView addSubview:self.time];

        self.videoPlayer = [[UIImageView alloc]initWithFrame:CGRectMake(self.imageView.frame.size.width/2 - 22.5, self.imageView.frame.size.height/2.0 - 89.0, 45.0, 45.0)];
        self.videoPlayer.image = [UIImage imageNamed:@"Video-Player.png"];
        [self.imageView addSubview:self.videoPlayer];
        
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.userBG];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
