//
//  CCGooglePlacesPlacesSearchQuery.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/6/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCGooglePlacesAutocompletePlace.h"
#import "CCGooglePlacesPlaceDetailQuery.h"

@interface CCGooglePlacesAutocompletePlace()
@property (nonatomic, retain, readwrite) NSString *name;
@property (nonatomic, retain, readwrite) NSString *reference;
@property (nonatomic, retain, readwrite) NSString *identifier;
@property (nonatomic, readwrite) SPGooglePlacesAutocompletePlaceType type;
@end

@implementation CCGooglePlacesAutocompletePlace

@synthesize name, reference, identifier, type;

+ (CCGooglePlacesAutocompletePlace *)placeFromDictionary:(NSDictionary *)placeDictionary {
    CCGooglePlacesAutocompletePlace *place = [[self alloc] init];
    place.name = [placeDictionary objectForKey:@"description"];
    place.reference = [placeDictionary objectForKey:@"reference"];
    place.identifier = [placeDictionary objectForKey:@"id"];
    place.type = SPPlaceTypeFromDictionary(placeDictionary);
    return place;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@,  %@, Identifier: %@, Type: %@",
            name, reference, identifier, SPPlaceTypeStringForPlaceType(type)];
}

- (CLGeocoder *)geocoder {
    if (!geocoder) {
        geocoder = [[CLGeocoder alloc] init];
    }
    return geocoder;
}

- (void)resolveEstablishmentPlaceToPlacemark:(SPGooglePlacesPlacemarkResultBlock)block {
    CCGooglePlacesPlaceDetailQuery *query = [CCGooglePlacesPlaceDetailQuery query];
    query.reference = self.reference;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
        if (error) {
            block(nil, nil, error);
        } else {
            NSString *addressString = [placeDictionary objectForKey:@"formatted_address"];
            
            [[self geocoder] geocodeAddressString:addressString completionHandler:^(NSArray *placemarks, NSError *error) {
                if (error) {
                    block(nil, nil, error);
                } else {
                    CLPlacemark *placemark = [placemarks onlyObject];
                    block(placemark, self.name, error);
                }
            }];
        }
    }];
}

- (void)resolveGecodePlaceToPlacemark:(SPGooglePlacesPlacemarkResultBlock)block {
    [[self geocoder] geocodeAddressString:self.name completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            block(nil, nil, error);
        } else {
            CLPlacemark *placemark = [placemarks onlyObject];
            block(placemark, self.name, error);
        }
    }];
}

- (void)resolveToPlacemark:(SPGooglePlacesPlacemarkResultBlock)block {
//    if (type == SPPlaceTypeGeocode) {
//        // Geocode places already have their address stored in the 'name' field.
//        [self resolveGecodePlaceToPlacemark:block];
//    } else {
        [self resolveEstablishmentPlaceToPlacemark:block];
//    }
}

@end
