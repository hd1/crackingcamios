//
//  CCViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "CCParentViewController.h"

@interface CCLoginViewController : CCParentViewController<UIScrollViewDelegate,FBLoginViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *loginWithFacebook;
- (IBAction)loginWithFacebook:(id)sender;

@end
