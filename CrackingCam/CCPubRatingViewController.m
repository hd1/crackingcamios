//
//  CCPubRatingViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/23/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "CCPubRatingViewController.h"
#import "SWRevealViewController/SWRevealViewController.h"
#import "CCGlobalData.h"
#import "CCMediaCollectionViewCell.h"
#import "CCGalleryViewController.h"
#import "CCRateViewController.h"
#import "CCHomeViewController.h"
#import "CCQueueViewController.h"

@interface CCPubRatingViewController ()
{
    SWRevealViewController *revealVC;
    NSMutableArray *allMedia;
    NSArray *userLocations;
    NSMutableArray *videos;
    NSMutableArray *images;
    NSArray *galleryMedia;
    NSDate *lastRefreshedDate;
    int type;
    BOOL hasPushedGalleryVC;
    NSIndexPath *selectedIndexPath;
    CLLocationManager *locationManager;
    NSInteger pubRating;
    BOOL isFromRemoteNotification;
}
@property (strong, nonatomic) IBOutlet UIView *bannerBackground;
- (IBAction)sortByAll:(id)sender;
- (IBAction)sortByVideos:(id)sender;
- (IBAction)sortByImages:(id)sender;
@end

@implementation CCPubRatingViewController

- (void)fetchMedia
{
    allMedia = [[NSMutableArray alloc]init];
    videos = [[NSMutableArray alloc]init];
    images = [[NSMutableArray alloc]init];
    
    [self showActivityIndicator];
    if ([CCGlobalData sharedGlobalData].selectedPlace.identifier != nil) {
        PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
        [locationsQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
        PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
        [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
        [mediaQuery whereKey:@"isPrivate" equalTo:@NO];
        [mediaQuery setLimit:300];
        [mediaQuery orderByDescending:@"createdAt"];
        [mediaQuery includeKey:@"user"];
        [mediaQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if (!error) {
                 NSInteger count = [objects count];
                 if (count>0) {
                     [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace = count;
                     lastRefreshedDate = [[objects objectAtIndex:0]createdAt];
                     for (PFObject *userLocationObject in objects) {
                         [allMedia addObject:userLocationObject];
                         if ([[userLocationObject valueForKey:@"isVideo"]boolValue]) {
                             [videos addObject:userLocationObject];
                         }
                         else
                         {
                             [images addObject:userLocationObject];
                         }
                     }
                     [self reloadCollectionView];
                 }
             }
             else
             {
                 [self showErrorAlertView:error.localizedDescription];
             }
             [self hideActivityIndicator];
         }];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isFromRemoteNotification = NO;
    locationManager = [[CLLocationManager alloc]init];
    type = 0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];
    [self.collectionView registerClass:[CCMediaCollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    UIImage *image = [UIImage imageNamed:@"camera-PubRating.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addRating) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBarBG.jpg"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *addRatingButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:addRatingButton, nil];
    
    self.scrollView.contentSize = CGSizeMake(320.0, 580.0);
    self.scrollView.scrollEnabled = YES;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:refreshControl];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(155.5, 137.0);
    layout.minimumLineSpacing = 3;
    layout.minimumInteritemSpacing = 3;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView.collectionViewLayout = layout;
    
    __block NSString *title;
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,250,40)];
    navTitle.textAlignment = NSTextAlignmentLeft;
    navTitle.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    navTitle.textColor = [UIColor whiteColor];

    if ([CCGlobalData sharedGlobalData].remoteNotificationLocationId) {
        [self setupSideMenuButton];
        isFromRemoteNotification = YES;
        [self.timingsLabel setText:@"Not Available"];
        [self showActivityIndicator];
        PFQuery *scoreQuery = [PFQuery queryWithClassName:@"UserLocation"];
        [scoreQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].remoteNotificationLocationId];
        [scoreQuery selectKeys:[NSArray arrayWithObjects:@"userObjectId",@"score",@"location",nil]];
        [scoreQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if(!error){
                 NSInteger averageScore = 0;
                 int friendCount = 0;
                 if ([objects count] > 0) {
                     PFGeoPoint *location = [[objects objectAtIndex:0]objectForKey:@"location"];

                     for (PFObject *object in objects) {
                         for (PFObject *friend in [CCGlobalData sharedGlobalData].friends) {
                             if ([[friend objectId] isEqualToString:[[object valueForKey:@"userObjectId"] objectId]]) {
                                 friendCount++;
                             }
                         }
                         averageScore = averageScore + [[object valueForKey:@"score"]integerValue];
                     }
                     averageScore = averageScore/[objects count];
                     pubRating = averageScore;
                     [CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace = friendCount;
                     
                     PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
                     [locationsQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].remoteNotificationLocationId];
                     PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
                     [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
                     [mediaQuery selectKeys:[NSArray arrayWithObjects:@"locationName",@"vicinity",nil]];
                     [mediaQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {

                         NSInteger reviewsCount = 0;
                         if(!error)
                         {
                             reviewsCount = [objects count];
                         }
                         else
                         {
                             [self showErrorAlertView:error.localizedDescription];
                         }
                         [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace = reviewsCount;
                         CCGooglePlace *googlePlace = [[CCGooglePlace alloc]init];
                         googlePlace.identifier = [CCGlobalData sharedGlobalData].remoteNotificationLocationId;
                         googlePlace.name = [[objects objectAtIndex:0]valueForKey:@"locationName"];
                         googlePlace.vicinity = [[objects objectAtIndex:0]valueForKey:@"vicinity"];
                         googlePlace.location = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                         [CCGlobalData sharedGlobalData].selectedPlace = googlePlace;
                         
                         title = [NSString stringWithFormat:@"%@ (%@)",[CCGlobalData sharedGlobalData].selectedPlace.name, [CCGlobalData sharedGlobalData].selectedPlace.vicinity];
                         navTitle.text = title;
                         
                         self.navigationItem.titleView = navTitle;
                         
                         PFQuery *queueQuery = [PFQuery queryWithClassName:@"UserQueue"];
                         [queueQuery whereKey:@"locationId" equalTo:googlePlace.identifier];
                         [queueQuery whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
                         [queueQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
                          {
                              if(!error && object != nil){
                                    [self.addToQueueButton setHidden:YES];
                              }
                          }];

                         [self.friendsCount setText:[NSString stringWithFormat:@"%ld Friends here",(long)[CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace]];
                         [self.reviewsCountLabel setText:[NSString stringWithFormat:@"%ld",(long)[CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace]];
                         
                         [self fetchMedia];
                         [self setupBannerView];
                         [CCGlobalData sharedGlobalData].remoteNotificationLocationId = nil;
                         [self hideActivityIndicator];
                     }];
                 }
                 else
                 {
                     self.scrollView.hidden = YES;
                     self.noRatingLabel.hidden = NO;
                     [self.addToQueueButton setHidden:YES];
                     [self hideActivityIndicator];
                 }
             }
             else
             {
                 [self hideActivityIndicator];
                 [self showErrorAlertView:error.localizedDescription];
             }
         }];
    }
    else
    {
        title = [NSString stringWithFormat:@"%@ (%@)",[CCGlobalData sharedGlobalData].selectedPlace.name, [CCGlobalData sharedGlobalData].selectedPlace.vicinity];
        navTitle.text = title;
        self.navigationItem.titleView = navTitle;
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back-Button-AboutUs.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
        self.navigationItem.leftBarButtonItem = backButtonItem;
    }
    
}

- (void)refreshData:(UIRefreshControl *)refreshControl
{
    PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
    [locationsQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
    PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
    [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
    [mediaQuery whereKey:@"isPrivate" equalTo:@NO];
    [mediaQuery orderByDescending:@"createdAt"];
    [mediaQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            if ([[object updatedAt] timeIntervalSinceDate:lastRefreshedDate] > 0) {
                [self fetchMedia];
            }
        }
        [refreshControl endRefreshing];
    }];
}
- (void)backButtonPressed
{
    if ([[self backViewController] isKindOfClass:[CCQueueViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[CCHomeViewController class]]) {
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
    }
}

- (void)setupBannerView
{
    if (pubRating) {
        [self setBannerViewWithScore:pubRating];
    }
    else
    {
        PFQuery *scoreQuery = [PFQuery queryWithClassName:@"UserLocation"];
        [scoreQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
        [scoreQuery selectKeys:[NSArray arrayWithObject:@"score"]];
        [scoreQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if(!error){
                 int averageScore = 0;
                 for (PFObject *object in objects) {
                     averageScore = averageScore + [[object valueForKey:@"score"]intValue];
                 }
                 averageScore = ceilf(averageScore/[objects count]);
                 [self setBannerViewWithScore:averageScore];
                 
             }
         }];
    }
}

- (void)setBannerViewWithScore:(NSInteger)score
{
    switch (score) {
        case 1:
            self.ratingImageView.image = [UIImage imageNamed:@"It's-Crap-Banner.png"];
            self.bannerBackground.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:17.0/255.0 blue:1/255.0 alpha:1.0];
            break;
        case 2:
            self.ratingImageView.image = [UIImage imageNamed:@"It's-Boring-Banner.png"];
            self.bannerBackground.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:185.0/255.0 blue:9.0/255.0 alpha:1.0];
            break;
        case 3:
            self.ratingImageView.image = [UIImage imageNamed:@"It's-Better-Banner.png"];
            self.bannerBackground.backgroundColor = [UIColor colorWithRed:46.0/255.0 green:106.0/255.0 blue:157.0/255.0 alpha:1.0];
            break;
        case 4:
            self.ratingImageView.image = [UIImage imageNamed:@"It's-Cracking-Banner.png"];
            self.bannerBackground.backgroundColor = [UIColor colorWithRed:85.0/255.0 green:197.0/255.0 blue:76.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    if (!isFromRemoteNotification) {
        if ( [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace == 0) {
            self.scrollView.hidden = YES;
            self.noRatingLabel.hidden = NO;
        }
        else
        {
            self.noRatingLabel.hidden = YES;
            self.scrollView.hidden = NO;
            
            [self.allButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            
            NSString *timings = [[CCGlobalData sharedGlobalData].selectedPlace.timings objectForKey:[CCGlobalData sharedGlobalData].currentDay];
            
            if (timings.length > 0) {
                [self.timingsLabel setText:[NSString stringWithFormat:@"%@",timings]];
            }
            else
            {
                [self.timingsLabel setText:@"Not Available"];
            }
            
            [self.friendsCount setText:[NSString stringWithFormat:@"%ld Friends here",(long)[CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace]];
            [self.reviewsCountLabel setText:[NSString stringWithFormat:@"%ld",(long)[CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace]];
            
            [self setupBannerView];
        }
        
        if ([CCGlobalData sharedGlobalData].isSelectedPlaceInQueue) {
            [self.addToQueueButton setHidden:YES];
        }
        
        if ([CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace != 0 && !hasPushedGalleryVC) {
            [self fetchMedia];
        }
    }
}

- (void)setupSideMenuButton
{
    revealVC = [self revealViewController];
    revealVC.delegate = self;
    [revealVC tapGestureRecognizer];
    UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-Friends"] style:UIBarButtonItemStylePlain target:revealVC action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = sideMenuButton;
    self.navigationItem.hidesBackButton = YES;
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (void)reloadCollectionView
{
    [self hideActivityIndicator];
    [self.collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (type) {
        case 0:
            return [allMedia count];
            break;
        case 1:
            return [videos count];
            break;
        case 2:
            return [images count];
            break;
        default:
            return 0;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"identifier";
    
    CCMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"launchImage"];
    NSArray *array;
    switch (type) {
        case 0:
            array = allMedia;
        break;
        case 1:
            array = videos;
            break;
        case 2:
            array = images;
            break;
        default:
            break;
    }
    PFObject *userLocationObject = [array objectAtIndex:indexPath.row];
    NSDate *updatedDate = [userLocationObject updatedAt];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:updatedDate];
    NSInteger minutes = timeInterval/60.0;
    if (minutes < 60) {
        [cell.time setText:[NSString stringWithFormat:@"%ld min ago",(long)minutes]];
    }
    else
    {
        [cell.time setText:[NSString stringWithFormat:@"%d hr ago",minutes/60]];
    }
    PFUser *user = [userLocationObject objectForKey:@"user"];
    cell.userName.text = [user valueForKey:@"fbUsername"];
    [cell.profilePicImageView setImageWithURL:[NSURL URLWithString:[user objectForKey:@"fbProfilePicURL"]] placeholderImage:nil options:SDWebImageProgressiveDownload];
    
    PFFile *dataFile = [userLocationObject objectForKey:@"dataFile"];
    if ([[userLocationObject valueForKey:@"isVideo"]boolValue]) {
        cell.videoPlayer.hidden = NO;
        PFFile *thumbnailFile = [userLocationObject objectForKey:@"videoThumbnail"];
        [cell.imageView setFile:thumbnailFile];
        [cell.imageView loadInBackground];
    }
    else
    {
        cell.videoPlayer.hidden = YES;
        [cell.imageView setFile:dataFile];
        [cell.imageView loadInBackground];
    }

    NSInteger score = [[userLocationObject valueForKey:@"score"]integerValue];
    switch (score) {
        case 1:
            cell.ratingView.image = [UIImage imageNamed:@"Crap-PubRating.png"];
            break;
        case 2:
            cell.ratingView.image = [UIImage imageNamed:@"Boring-PubRating.png"];
            break;
        case 3:
            cell.ratingView.image = [UIImage imageNamed:@"Better-PubRating.png"];
            break;
        case 4:
            cell.ratingView.image = [UIImage imageNamed:@"Cracking-PubRating.png"];
            break;
            
        default:
            break;
    }

    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (type) {
        case 0:
            galleryMedia = allMedia;
            break;
        case 1:
            galleryMedia = videos;
            break;
        case 2:
            galleryMedia = images;
            break;
        default:
            break;
    }
    selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"PubRating_Gallery" sender:self];
}

#pragma mark - Navigation

- (void)addRating{
    
    CLLocation *selectedPlaceLocation = [[CLLocation alloc]initWithLatitude:[CCGlobalData sharedGlobalData].selectedPlace.location.latitude longitude:[CCGlobalData sharedGlobalData].selectedPlace.location.longitude];
    CLLocationDistance distance = [selectedPlaceLocation distanceFromLocation:locationManager.location];
    
    if (distance <= 1609.344) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        PFQuery *userLocation = [PFQuery queryWithClassName:@"UserLocation"];
        [userLocation whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
        [userLocation whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
        PFQuery *userLocationObjectsQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
        [userLocationObjectsQuery whereKey:@"userLocation" matchesQuery:userLocation];
        
        [userLocationObjectsQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
            if (!error) {
                if (number == 6) {
                    [[[UIAlertView alloc]initWithTitle:nil message:@"You already have posted 6 times today for this location" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                }
                else
                {
                    NSDate *date = [userDefaults objectForKey:@"lastPublishedDate"];
                    if (date != nil) {
                        NSTimeInterval timeIntervalSinceLastPost = [[NSDate date]timeIntervalSinceDate:date];
                        NSInteger minutes = timeIntervalSinceLastPost/60.0;
                        if (minutes >= 15) {
                            if (minutes > 1440) {
                                NSString *appDomain = [[NSBundle mainBundle]bundleIdentifier];
                                [userDefaults removePersistentDomainForName:appDomain];
                            }
                            [self performSegueWithIdentifier:@"PubRating_Rate" sender:self];
                        }
                        else
                        {
                            [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%ld min left for your next post",15 - (long)minutes] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                        }
                    }
                    else
                    {
                        [self performSegueWithIdentifier:@"PubRating_Rate" sender:self];
                    }
                }
            }
            else
            {
                [self showErrorAlertView:error.localizedDescription];
            }
        }];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you are at this venue?  Your current location seems a little far."] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }
}

- (IBAction)sortByAll:(id)sender {
    type = 0;
    [self.allButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.videosButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.imagesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.collectionView reloadData];
}

- (IBAction)sortByVideos:(id)sender {
    type = 1;
    [self.allButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.videosButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.imagesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.collectionView reloadData];
}

- (IBAction)sortByImages:(id)sender {
    type = 2;
    [self.allButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.videosButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.imagesButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.collectionView reloadData];
}
- (IBAction)addToQueue:(id)sender {
    [self.addToQueueButton setTitle:@"Added to Queue" forState:UIControlStateNormal];
    [self.addToQueueButton setImage:nil forState:UIControlStateNormal];
    [self.addToQueueButton setEnabled:NO];
    [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = YES;
    
    CCGooglePlace *place = [CCGlobalData sharedGlobalData].selectedPlace;
    PFObject *queue = [PFObject objectWithClassName:@"UserQueue"];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setObject:place.name  forKey:@"placeName"];
    [dictionary setObject:place.vicinity forKey:@"vicinity"];
    if (place.timings != nil) {
        [dictionary setObject:place.timings forKey:@"timings"];
    }
    [queue setObject:dictionary  forKey:@"locationDetails"];
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:place.location.latitude longitude:place.location.longitude];
    [queue setObject:geoPoint forKey:@"location"];
    [queue setObject:[PFUser currentUser] forKey:@"userObjectId"];
    [queue setObject:place.identifier forKey:@"locationId"];
    [queue saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
        if (succeeded) {
             [UIView animateWithDuration:1 animations:^{
                 [self.addToQueueButton setHidden:YES];
             }];
         }
         else
         {
             [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = NO;
             [self.addToQueueButton setTitle:@"Add to Queue" forState:UIControlStateNormal];
             [self.addToQueueButton setImage:[UIImage imageNamed:@"Plus-Icon.png"] forState:UIControlStateNormal];
             [self.addToQueueButton setEnabled:YES];
         }
     }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PubRating_Gallery"]) {
        CCGalleryViewController *galleryVC = [segue destinationViewController];
        galleryVC.media = galleryMedia;
        galleryVC.indexPath = selectedIndexPath;
        hasPushedGalleryVC = YES;
    }
    else
    {
        hasPushedGalleryVC = NO;
    }
}

@end
