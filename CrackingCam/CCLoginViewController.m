//
//  CCViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Parse/Parse.h>

#import "CCLoginViewController.h"
#import "CCHomeViewController.h"
#import "CCAppDelegate.h"
#import "CCGlobalData.h"
#import "MBProgressHUD.h"

@interface CCLoginViewController ()
{
    NSArray *pageViews;
    NSString *profilePictureURL;
    NSMutableData *imageData;
}
@property (strong, nonatomic) IBOutlet UILabel *labelOne;
@property (strong, nonatomic) IBOutlet UILabel *labelTwo;
@property (strong, nonatomic) IBOutlet UILabel *labelThree;
@end

@implementation CCLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBarHidden = YES;
    [[PFUser currentUser]refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (error) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"logoutUser" object:nil];
        }
    }];

    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [self performSegueWithIdentifier:@"loginToHome" sender:self];
    }
    else{
        [self loadScrollView];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadScrollView
{
    NSMutableAttributedString *labelOneText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"\"Know the venue, find the cracking score of the venue\""]];
    [labelOneText addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(26,8)];
    [self.labelOne setAttributedText:labelOneText];
    
    NSMutableAttributedString *labelTwoText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"\"See if your facebook friends are having a cracking time\""]];
    [labelTwoText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:46.0/255.0 green:106.0/255.0 blue:157.0/255.0 alpha:1.0] range:NSMakeRange(13,8)];
    [labelTwoText addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(43,8)];
    [self.labelTwo setAttributedText:labelTwoText];
    
    NSMutableAttributedString *labelThreeText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"\"Waste less time looking, prepare for the cracking locations\""]];
    [labelThreeText addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(42,8)];
    [self.labelThree setAttributedText:labelThreeText];
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.scrollView.frame) * 3, CGRectGetHeight(self.scrollView.frame))];
    self.scrollView.delegate = self;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
}

- (IBAction)loginWithFacebook:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [PFFacebookUtils logInWithPermissions:[NSArray arrayWithObject:@"user_friends"] block:^(PFUser *user, NSError *error) {
        if (!user) {
            if (!error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:error.fberrorUserMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
        }
        else
        {
            if ([FBSession.activeSession.permissions indexOfObject:@"user_friends"] == NSNotFound) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Permission Denied" message:@"Please give permission to access your facebook friends list." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                alert.tag = 2;
                [alert show];
            }
            else
            {
                [self saveUserDetails];
                
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self hideActivityIndicator];
    }];
  
}

- (void)saveUserDetails
{
    if ([PFUser currentUser].isNew) {
        FBRequest *request = [FBRequest requestForMe];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if(!error)
            {
                NSString *facebookId = [result objectForKey:@"id"];
                [[PFUser currentUser] setObject:facebookId forKey:@"fbId"];
                [[PFUser currentUser] setObject:[result objectForKey:@"name"] forKey:@"fbUsername"];
                profilePictureURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1",facebookId];
                [[PFUser currentUser] setObject:profilePictureURL forKey:@"fbProfilePicURL"];
                [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    [[PFInstallation currentInstallation]setObject:[PFUser currentUser] forKey:@"user"];
                    [[PFInstallation currentInstallation]saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                if (!error) {
                                    NSArray *friendObjects = [result objectForKey:@"data"];
                                    for (NSDictionary *friendObject in friendObjects) {
                                        [[CCGlobalData sharedGlobalData].fbFriendsIds addObject:[friendObject objectForKey:@"id"]];
                                    }
                                    [self performSegueWithIdentifier:@"loginToHome" sender:self];
                                    [self sendPushNotificationsToFacebookFriends];
                                }
                                else
                                {
                                    [self showErrorAlertView:error.localizedDescription];
                                }
                            }];

                        }
                    }];
                }];
            }
            else [self showErrorAlertView:error.localizedDescription];
        }];
    }
    else
    {
        [self performSegueWithIdentifier:@"loginToHome" sender:self];
    }
}

- (void)sendPushNotificationsToFacebookFriends
{
    PFQuery *friendQuery = [PFUser query];
    [friendQuery whereKey:@"fbId" containedIn:[CCGlobalData sharedGlobalData].fbFriendsIds];
    friendQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
    friendQuery.maxCacheAge = 5 * 60;
    [friendQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(!error)
        {
            if ([[CCGlobalData sharedGlobalData].friends count] > 0) {
                [[CCGlobalData sharedGlobalData].friends removeAllObjects];
            }
            [[CCGlobalData sharedGlobalData].friends addObjectsFromArray:objects];
            PFQuery *query = [PFInstallation query];
            [query whereKey:@"user" containedIn:[CCGlobalData sharedGlobalData].friends];
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:query];
            NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@ has joined you on CrackingCam!",[[PFUser currentUser] valueForKey:@"fbUsername"]], @"alert",
                                  @"Increment", @"badge",
                                   @"",@"sound",
                                  [NSString stringWithFormat:@"%@",[[PFUser currentUser]objectId]],@"NewFriend",
                                    nil];
            [push setData:data];
            [push sendPushInBackground];
        }
        else{
            [self showErrorAlertView:error.localizedDescription];
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 2)
    {
        [[FBSession activeSession] requestNewReadPermissions:[NSArray arrayWithObject:@"user_friends"] completionHandler:^(FBSession *session, NSError *error)
         {
             if(!error){
                 if ([session.permissions indexOfObject:@"user_friends"] == NSNotFound) {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Permission Denied" message:@"Please give permission to access your facebook friends list." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                 }
                 else
                 {
                     [self saveUserDetails];
                 }
             }
             else
             {
                 [self showErrorAlertView:error.fberrorUserMessage];
             }
         }];
    }
}

@end
