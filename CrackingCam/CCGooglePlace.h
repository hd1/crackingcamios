//
//  CCGooglePlace.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/6/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CCGooglePlace : NSObject
+ (CCGooglePlace *)placeFromDictionary:(NSDictionary *)placeDictionary;

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic) CLLocationCoordinate2D location;
@property (nonatomic, retain) NSString *reference;
@property (nonatomic, retain) NSDictionary *timings;
@property (nonatomic) NSString *vicinity;
@property NSInteger friendsCount;
@property NSInteger score;
@end
