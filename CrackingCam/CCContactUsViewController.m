//
//  CCContactUsViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCContactUsViewController.h"
#import "SWRevealViewController.h"
#import <Parse/Parse.h>

@interface CCContactUsViewController ()
{
    SWRevealViewController *revealVC;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UITextField *activeTextField;
@end

@implementation CCContactUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Contact-BG.png"]]];
    [self.scrollView setContentSize:CGSizeMake(320.0, 490.0)];
    
    [self.messageTextView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    self.messageTextView.layer.borderWidth = 0.5;
    
    [self.nameTextField.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [self.nameTextField.layer setBorderWidth:0.5];
    
    [self.mobileNumberTextField.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [self.mobileNumberTextField.layer setBorderWidth:0.5];

    [self.emailTextField.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [self.emailTextField.layer setBorderWidth:0.5];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self registerForKeyboardNotifications];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma mark - Keyboard Handling

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (self.activeTextField != nil) {
        if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
            [self.scrollView scrollRectToVisible:self.activeTextField.frame animated:YES];
        }
    }
    else
    {
        if (!CGRectContainsPoint(aRect, self.messageTextView.frame.origin) ) {
            [self.scrollView scrollRectToVisible:self.messageTextView.frame animated:YES];
        }
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - TextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
    if (textField == self.nameTextField && [textField.text isEqualToString:@""]) {
            self.nameTextField.text = @"Name";
    }
    else if (textField == self.emailTextField && [textField.text isEqualToString:@""])
    {
            self.emailTextField.text = @"Email Address";
    }
    else if (textField == self.mobileNumberTextField && [textField.text isEqualToString:@""]) {
            self.mobileNumberTextField.text = @"Mobile Number";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.nameTextField) {
        if ([textField.text isEqualToString:@""]) {
            self.nameTextField.text = @"Name";
        }
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField)
    {
        if ([textField.text isEqualToString:@""]) {
            self.emailTextField.text = @"Email Address";
        }
        [self.mobileNumberTextField becomeFirstResponder];
    }

    if (textField == self.mobileNumberTextField) {
        if ([textField.text isEqualToString:@""]) {
            self.mobileNumberTextField.text = @"Mobile Number";
        }
        return YES;
    }
    return NO;
}

#pragma mark - TextView Delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text hasPrefix:@"*Please enter"]) {
        _messageTextView.text = @"";
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(_messageTextView.text.length == 0){
        _messageTextView.text = @"*Please enter your message";
        [_messageTextView resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(_messageTextView.text.length == 0){
            _messageTextView.text = @"*Please enter your message";
            [_messageTextView resignFirstResponder];
        }
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(_messageTextView.text.length == 0){
        _messageTextView.text = @"*Please enter your message";
    }
    [self.messageTextView resignFirstResponder];
    [self.nameTextField becomeFirstResponder];
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.scrollView endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)submit:(id)sender {
    if (self.nameTextField.text.length != 0 && ![self.nameTextField.text isEqualToString:@"Name"]) {
        if ([self validateEmailWithString:self.emailTextField.text]) {
            if (self.messageTextView.text.length !=0 && ![self.messageTextView.text isEqualToString:@"*Please enter your message"]) {
                PFObject *object = [PFObject objectWithClassName:@"UserFeedback"];
                object[@"name"] = self.nameTextField.text;
                object[@"email"] = self.emailTextField.text;
                if (self.mobileNumberTextField.text.length != 0 && self.mobileNumberTextField) {
                    object[@"phoneNumber"] = self.mobileNumberTextField.text;
                }
                object[@"message"] = self.messageTextView.text;
                [object saveEventually];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                UIAlertView *messageAlertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a message" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                messageAlertView.tag = 0;
                [messageAlertView show];
            }
        }
        else
        {
            UIAlertView *emailAlertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            emailAlertView.tag = 1;
            [emailAlertView show];
        }
    }
    else
    {
       UIAlertView *nameAlertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter your name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        nameAlertView.tag = 2;
        [nameAlertView show];
    }
    
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case 0:
            [self.messageTextView becomeFirstResponder];
            break;
        case 1:
            [self.emailTextField becomeFirstResponder];
            break;
        case 2:
            [self.nameTextField becomeFirstResponder];
            break;
        default:
            break;
    }
    
}

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Autorotation

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
