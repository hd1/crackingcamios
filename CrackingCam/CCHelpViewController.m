//
//  CCHelpViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/19/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCHelpViewController.h"

@interface CCHelpViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *faqWebView;

@end

@implementation CCHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];
    [self.faqWebView setBackgroundColor:[UIColor clearColor]];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"HelpHTML" ofType:@"html"];
    NSURL *url=[NSURL fileURLWithPath:htmlFile];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    [self.faqWebView loadRequest:request];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NO];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
