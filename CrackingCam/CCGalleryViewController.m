//
//  CCGalleryViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/26/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

#import "CCGalleryViewController.h"
#import "CCGalleryViewCell.h"

@interface CCGalleryViewController ()
{
}
@end
static NSString *identifier = @"identifier";

@implementation CCGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]]];
    self.navigationController.navigationBarHidden = YES;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    layout.sectionInset = UIEdgeInsetsZero;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.galleryCollectionView.collectionViewLayout = layout;
    [self.galleryCollectionView registerClass:[CCGalleryViewCell class] forCellWithReuseIdentifier:identifier];
    if (_indexPath != nil) {
        [self.galleryCollectionView scrollToItemAtIndexPath:_indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.media count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CCGalleryViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.closeButton addTarget:self action:@selector(dismissGallery:) forControlEvents:UIControlEventTouchUpInside];
    
    PFObject *userLocationObject = [self.media objectAtIndex:indexPath.row];
    
    PFUser *user = [userLocationObject objectForKey:@"user"];
    [cell.userProfilePic setImageWithURL:[NSURL URLWithString:[user objectForKey:@"fbProfilePicURL"]] placeholderImage:nil options:SDWebImageProgressiveDownload];
    [cell.userName setText:[user objectForKey:@"fbUsername"]];
    
    NSDate *updatedDate = [userLocationObject updatedAt];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:updatedDate];
    NSInteger minutes = timeInterval/60.0;
    if (minutes < 60) {
        [cell.time setText:[NSString stringWithFormat:@"%ld min ago",(long)minutes]];
    }
    else
    {
        [cell.time setText:[NSString stringWithFormat:@"%ld hr ago",(long)minutes/60]];
    }
    NSString *caption = [userLocationObject objectForKey:@"comment"];
    if (caption.length > 0) {
        [cell.captionLabel setHidden:NO];
        [cell.captionLabel setText:caption];
    }
    else{
        [cell.captionLabel setHidden:YES];
    }
    
    PFFile *dataFile = [userLocationObject objectForKey:@"dataFile"];
    if ([[userLocationObject valueForKey:@"isVideo"]boolValue]) {
        cell.videoPlayer.hidden = NO;
        PFFile *thumbnailFile = [userLocationObject objectForKey:@"videoThumbnail"];
        [cell.imageView setFile:thumbnailFile];
        [cell.imageView loadInBackground];
    }
    else
    {
        cell.videoPlayer.hidden = YES;
        cell.imageView.file = dataFile;
        [cell.imageView loadInBackground];
    }
    NSInteger score = [[userLocationObject valueForKey:@"score"]integerValue];
    switch (score) {
        case 1:
            cell.ratingImage.image = [UIImage imageNamed:@"Crap-Gallery.png"];
            [cell.ratingLabel setText:@"It's Crap"];
            break;
        case 2:
            cell.ratingImage.image = [UIImage imageNamed:@"Boring-Gallery.png"];
            [cell.ratingLabel setText:@"It's Boring"];
            break;
        case 3:
            cell.ratingImage.image = [UIImage imageNamed:@"Better-PubRating.png"];
            [cell.ratingLabel setText:@"It's Alright"];
            break;
        case 4:
            cell.ratingImage.image = [UIImage imageNamed:@"Cracking-Gallery.png"];
            [cell.ratingLabel setText:@"It's Cracking"];
            break;
            
        default:
            break;
    }
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PFObject *userLocationObject = [self.media objectAtIndex:indexPath.row];
    PFFile *dataFile = [userLocationObject objectForKey:@"dataFile"];
    if ([[userLocationObject valueForKey:@"isVideo"]boolValue]) {
        MPMoviePlayerViewController *moviePlayerViewController = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL URLWithString:dataFile.url]];
        [moviePlayerViewController.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
        [moviePlayerViewController.moviePlayer setShouldAutoplay:YES];
        [moviePlayerViewController.moviePlayer setFullscreen:NO animated:YES];
        [moviePlayerViewController.moviePlayer setScalingMode:MPMovieScalingModeNone];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackStateDidChange:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:moviePlayerViewController];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayerViewController];
        [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];

    }
}

#pragma mark - MPMoviePlayer Notification Methods

-(void)moviePlaybackStateDidChange:(NSNotification *)notification
{
    MPMoviePlayerViewController *moviePlayerViewController = [notification object];
    
    if (moviePlayerViewController.moviePlayer.loadState == MPMovieLoadStatePlayable &&
        moviePlayerViewController.moviePlayer.playbackState != MPMoviePlaybackStatePlaying)
    {
        [moviePlayerViewController.moviePlayer play];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                  object:moviePlayerViewController];
    moviePlayerViewController = nil;
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    MPMoviePlayerViewController *moviePlayerViewController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerViewController];
    [self dismissMoviePlayerViewControllerAnimated];
    moviePlayerViewController = nil;
}

- (IBAction)dismissGallery:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
