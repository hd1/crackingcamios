//
//  CCAboutUsViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface CCAboutUsViewController : UIViewController<MFMailComposeViewControllerDelegate>
- (IBAction)email:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButtonPressed:(id)sender;
@end
