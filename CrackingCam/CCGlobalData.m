//
//  CCGlobalData.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCGlobalData.h"

@implementation CCGlobalData

+ (CCGlobalData *)sharedGlobalData
{
    static dispatch_once_t p = 0;
    
    __strong static CCGlobalData *_sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

@end
