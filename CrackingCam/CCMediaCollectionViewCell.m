//
//  CCMediaCollectionViewCell.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/26/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCMediaCollectionViewCell.h"

@implementation CCMediaCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[PFImageView alloc]initWithFrame:CGRectMake(0.0, 0.0, 155.5, 137.0)];
        
        self.profilePicImageView = [[PFImageView alloc]initWithFrame:CGRectMake(5.0, 97.0, 35.0, 35.0)];
        self.profilePicImageView.layer.cornerRadius = 5.0;
        self.profilePicImageView.layer.masksToBounds = YES;
        self.profilePicImageView.layer.borderWidth   = 1.0;
        self.profilePicImageView.layer.borderColor   = [UIColor whiteColor].CGColor;
        
        self.userName = [[UILabel alloc]initWithFrame:CGRectMake(45.0, 100.0, 75.0, 20.0)];
        self.userName.textColor = [UIColor whiteColor];
        self.userName.font      = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
        
        self.time     = [[UILabel alloc]initWithFrame:CGRectMake(45.0, 117.0, 75.0, 20.0)];
        self.time.textColor = [UIColor whiteColor];
        self.time.font      = [UIFont fontWithName:@"Helvetica" size:8.0];

        self.ratingView = [[UIImageView alloc]initWithFrame:CGRectMake(122.5, 102.0, 30.0, 30.0)];
        self.ratingView.layer.cornerRadius = self.ratingView.frame.size.width/2.0;
        self.ratingView.layer.masksToBounds = YES;
        
        self.videoPlayer = [[UIImageView alloc]initWithFrame:CGRectMake(self.imageView.frame.size.width/2.0-15.0, self.imageView.frame.size.height/2.0-15.0, 30.0, 30.0)];
        self.videoPlayer.image = [UIImage imageNamed:@"Video-Player.png"];
        
        [self.imageView addSubview:self.videoPlayer];
        [self.imageView addSubview:self.ratingView];
        [self.imageView addSubview:self.profilePicImageView];
        [self.imageView addSubview:self.userName];
        [self.imageView addSubview:self.time];
        
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
