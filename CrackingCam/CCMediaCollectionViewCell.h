//
//  CCMediaCollectionViewCell.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/26/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface CCMediaCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) PFImageView *imageView;
@property (nonatomic, strong) PFImageView *profilePicImageView;
@property (nonatomic, strong) UIImageView *ratingView;
@property (nonatomic, strong) UIImageView *videoPlayer;

@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *time;

@end
