//
//  CCFriendsActivityCollectionViewCell.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/20/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCFriendsActivityCollectionViewCell.h"

@implementation CCFriendsActivityCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[PFImageView alloc]initWithFrame:CGRectMake(0.0, 0.0, 155.5, 139.0)];
        
       
        self.pubName = [[UILabel alloc]initWithFrame:CGRectMake(5.0, self.imageView.frame.size.height - 35.0, 120.0, 21.0)];
        self.pubName.textColor = [UIColor whiteColor];
        self.pubName.font      = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
        self.time     = [[UILabel alloc]initWithFrame:CGRectMake(5.0, self.pubName.frame.origin.y+self.pubName.frame.size.height-8.0 , 75.0, 21.0)];
        self.time.textColor = [UIColor whiteColor];
        self.time.font      = [UIFont fontWithName:@"Helvetica" size:8.0];
        
        self.ratingView = [[UIImageView alloc]initWithFrame:CGRectMake(120.5, 105.0, 30.0, 30.0)];
        self.ratingView.layer.cornerRadius = self.ratingView.frame.size.width/2.0;
        self.ratingView.layer.masksToBounds = YES;
        
        self.videoPlayer = [[UIImageView alloc]initWithFrame:CGRectMake(self.imageView.frame.size.width/2.0-15.0, self.imageView.frame.size.height/2.0-15.0, 30.0, 30.0)];
        self.videoPlayer.image = [UIImage imageNamed:@"Video-Player.png"];
        
        [self.imageView addSubview:self.videoPlayer];
        [self.imageView addSubview:self.ratingView];
        [self.imageView addSubview:self.pubName];
        [self.imageView addSubview:self.time];
        
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
