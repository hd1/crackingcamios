//
//  CCFriendsListViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/20/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCFriendsListViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageOperation.h>
#import <SDWebImage/SDWebImageDownloader.h>
#import <Social/Social.h>

#import "SWRevealViewController/SWRevealViewController.h"
#import "CCGlobalData.h"
#import "CCGalleryViewController.h"
#import "CCFriendsActivityViewController.h"
#import "CCFriendsTableViewCell.h"

@interface CCFriendsListViewController ()
{
    SWRevealViewController *revealVC;
    NSMutableArray *friends;
    NSArray *media;
}
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (strong, nonatomic) IBOutlet UITableView *friendsTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation CCFriendsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    revealVC = [self revealViewController];
    [revealVC tapGestureRecognizer];
    UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-Friends"] style:UIBarButtonItemStylePlain target:revealVC action:@selector(revealToggle:)];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];
    self.navigationItem.leftBarButtonItem = sideMenuButton;
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    navTitle.textAlignment = NSTextAlignmentLeft;
    navTitle.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    navTitle.textColor = [UIColor whiteColor];
    
    if(_isFromPub)
    {
        navTitle.text = @"Friends Here";
    }
    else
    {
        friends = [[NSMutableArray alloc]init];
        [friends addObject:[PFUser currentUser]];
        [self findFriends];
        navTitle.text = @"My Friends";
    }
    self.navigationItem.titleView = navTitle;
    self.navigationItem.hidesBackButton = YES;
    
    [self.searchDisplayController.searchBar setImage:[UIImage imageNamed:@"Search-Icon-unselected.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.searchDisplayController.searchBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Sticky-Footer-BG.png"]]];
    [self.searchDisplayController.searchBar setTintColor:[UIColor grayColor]];
    
    self.friendsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self setupNavigationBar];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    NSInteger row = [friends indexOfObject:[CCGlobalData sharedGlobalData].fbFriendObjectId];
    [self.friendsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
    [CCGlobalData sharedGlobalData].fbFriendObjectId = nil;
}
- (void)setupNavigationBar
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBarBG.jpg"] forBarMetrics:UIBarMetricsDefault];
    self.searchBar.hidden = YES;
    self.searchDisplayController.searchBar.placeholder = @"Search by friends name";
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchFriends)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton, nil];
}

- (void)searchFriends
{
    self.searchDisplayController.searchBar.hidden = NO;
    [self.searchDisplayController.searchBar becomeFirstResponder];
}
- (void)findFriends
{
    if ([[CCGlobalData sharedGlobalData].friends count] == 0) {
        [self showActivityIndicator];
        FBRequest *request = [FBRequest requestForMyFriends];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                NSArray *friendObjects = [result objectForKey:@"data"];
                NSMutableArray *friendIds = [NSMutableArray arrayWithCapacity:friendObjects.count];
                for (NSDictionary *friendObject in friendObjects) {
                    [friendIds addObject:[friendObject objectForKey:@"id"]];
                }
                PFQuery *friendQuery = [PFUser query];
                [friendQuery whereKey:@"fbId" containedIn:friendIds];
                [friendQuery selectKeys:[NSArray arrayWithObjects:@"fbProfilePicURL",@"fbUsername", nil]];
                friendQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
                friendQuery.maxCacheAge = 5 * 60;
                [friendQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
                    if(!error)
                    {
                        [friends addObjectsFromArray:objects];
                        [self.friendsTableView reloadData];
                    }
                    else
                    {
                        [self showErrorAlertView:error.localizedDescription];
                    }
                    [self hideActivityIndicator];
                }];
            }
            else
            {
                [self showErrorAlertView:error.fberrorUserMessage];
            }
        }];
    }
    else
    {
        [friends addObjectsFromArray:[CCGlobalData sharedGlobalData].friends];
        [self.friendsTableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
      return [self.searchResults count];
    }
    else
    {
    if(_isFromPub) return [self.friendsAtCurrentPub count]+1;
        else return [friends count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
        CCFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
          cell =  [[[NSBundle mainBundle] loadNibNamed:@"CCFriendsTableViewCell" owner:self options:nil]objectAtIndex:0];

        }
    
    PFUser *user;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        user = [self.searchResults objectAtIndex:indexPath.row];
    }
    else if (tableView == self.friendsTableView)
    {
        if(_isFromPub)
        {
            if (indexPath.row == 0) {
                [cell.reviewsCountLabel setHidden:YES];
                [cell.timeLabel setHidden:YES];
                [cell.userName setText:[NSString stringWithFormat:@"%@",[CCGlobalData sharedGlobalData].selectedPlace.name]];
                [cell setBackgroundColor:[UIColor orangeColor]];
                cell.userInteractionEnabled = NO;
                return  cell;
            }
            user = (PFUser *)[[self.friendsAtCurrentPub objectAtIndex:indexPath.row-1]fetchIfNeeded];
        }
        else
        {
            user = [friends objectAtIndex:indexPath.row];
        }
    }
    NSString *userName = [user objectForKey:@"fbUsername"];
    [cell.userName setText:userName];
    [cell.userProfilePic setImageWithURL:[NSURL URLWithString:[user objectForKey:@"fbProfilePicURL"]] placeholderImage:nil options:SDWebImageProgressiveDownload];
    if(_isFromPub)
    {
            [cell.detailsTextLabel setHidden:YES];
            PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
            [locationsQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
            [locationsQuery whereKey:@"userObjectId" equalTo:user];
            PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
            [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
            [mediaQuery setCachePolicy:kPFCachePolicyCacheElseNetwork];
            [mediaQuery setMaxCacheAge:2*60];
            [mediaQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error)
             {
                 [cell.reviewsCountLabel setText:[NSString stringWithFormat:@"%d Reviews",number]];
             }];
    }
    else
    {
            [cell.timeLabel setHidden:YES];
            [cell.reviewsCountLabel setHidden:YES];
    }
    PFQuery *userLocation = [PFQuery queryWithClassName:@"UserLocation"];
    [userLocation whereKey:@"userObjectId" equalTo:user];
    [userLocation setCachePolicy:kPFCachePolicyCacheElseNetwork];
    [userLocation setMaxCacheAge:5*60];
    [userLocation getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
     {
         if (!error) {
             NSDate *updatedDate = [object updatedAt];
             NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:updatedDate];
             NSInteger minutes = timeInterval/60.0;
             if (minutes < 60) {
                 if (_isFromPub) {
                     [cell.timeLabel setText:[NSString stringWithFormat:@"%ld mins ago",(long)minutes]];
                 }
                 else
                     [cell.detailsTextLabel setText:[NSString stringWithFormat:@"%ld mins ago",(long)minutes]];
             }
             else
             {
                 if (_isFromPub) {
                     [cell.timeLabel setText:[NSString stringWithFormat:@"%d hrs ago",minutes/60]];
                 }
                 else
                     [cell.detailsTextLabel setText:[NSString stringWithFormat:@"%d hrs ago",minutes/60]];
             }
         }
     }];
   
   return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFQuery *query = [PFQuery queryWithClassName:@"UserLocationObjects"];
    [query orderByDescending:@"createdAt"];
    [query includeKey:@"user"];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        [query whereKey:@"user" equalTo:[self.searchResults objectAtIndex:indexPath.row]];
    }
    else
    {
        if(_isFromPub)
        {
            PFQuery *userLocationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
            [userLocationsQuery whereKey:@"userObjectId" equalTo:[_friendsAtCurrentPub objectAtIndex:indexPath.row-1.0]];
            [userLocationsQuery whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
            [query whereKey:@"userLocation" matchesQuery:userLocationsQuery];
        }
        else
        {
            [query whereKey:@"user" equalTo:[friends objectAtIndex:indexPath.row]];
        }
    }
    
    [self showActivityIndicator];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    {
        [self hideActivityIndicator];
        if(!error)
        {
            if ([objects count]>0) {
                media = [NSArray arrayWithArray:objects];
                [self performSegueWithIdentifier:@"Friends_FriendsActivity" sender:self];
            }
            else
            {
                UIAlertView *message = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"No activity to show"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [message show];
            }
        }
        else
        {
            [self showErrorAlertView:error.localizedDescription];
        }
    }];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self.searchResults removeAllObjects];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"fbUsername contains[cd] %@",
                                    searchString];
    if(_isFromPub)
    {
        self.searchResults = [NSMutableArray arrayWithArray:[_friendsAtCurrentPub filteredArrayUsingPredicate:resultPredicate]];
    }
    else{
        self.searchResults = [NSMutableArray arrayWithArray:[friends filteredArrayUsingPredicate:resultPredicate]];
    }
    return YES;
}


- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    self.searchBar.hidden = YES;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Friends_FriendsActivity"]) {
        CCFriendsActivityViewController *friendsActivityVC = [segue destinationViewController];
        friendsActivityVC.media = media;
    }
}
- (IBAction)shareWithFriends:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:@"Check out CrackingCam! The app where CRACKING parties don't go unnoticed ;)"];
        [controller addURL:[NSURL URLWithString:@"http://www.crackingcam.com/"]];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Account" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


@end
