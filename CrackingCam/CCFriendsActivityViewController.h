//
//  CCFriendsActivityViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/21/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCFriendsActivityViewController : UIViewController

@property (nonatomic, strong) NSArray *media;

@end
