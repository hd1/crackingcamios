//
//  CCFriendsListViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/20/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CCParentViewController.h"
@interface CCFriendsListViewController : CCParentViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate, NSURLConnectionDataDelegate>

@property BOOL isFromPub;
@property (nonatomic,strong) NSMutableArray *friendsAtCurrentPub;

@end
