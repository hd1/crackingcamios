//
//  CCCustomInfoWindow.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/7/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCCustomInfoWindow.h"
#import "CCGlobalData.h"
#import <Parse/Parse.h>
#import "CCGooglePlace.h"
@implementation CCCustomInfoWindow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)layoutSubviews {
    [self.backgroundView.layer setCornerRadius:5.0];
    [self.backgroundView.layer setMasksToBounds:YES];
    [self.backgroundView.layer setBorderWidth:0.8];
    [self.backgroundView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
}
@end
