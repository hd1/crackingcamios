//
//  CCParentViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/2/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCParentViewController.h"

@interface CCParentViewController ()
{
    UIPanGestureRecognizer *panGestureRecognizer;
}
@end

@implementation CCParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.activityIndicator setCenter:CGPointMake(self.navigationController.view.frame.size.width/2.0, self.navigationController.view.frame.size.height/2.0)];
    
    self.loadingView = [[UIView alloc]initWithFrame:CGRectMake(self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height)];
    [self.loadingView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    self.loadingView.hidden = YES;
    [self.navigationController.view addSubview:self.loadingView];
    [self.navigationController.view addSubview:self.activityIndicator];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)showActivityIndicator
{
   self.loadingView.hidden = NO;
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator
{
    [self.view removeGestureRecognizer:panGestureRecognizer];
    [self.activityIndicator stopAnimating];
    self.loadingView.hidden = YES;
}

- (void)showErrorAlertView:(NSString *)error
{
    [[[UIAlertView alloc]initWithTitle:@"Network Error" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
}

@end
