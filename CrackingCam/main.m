     //
//  main.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
