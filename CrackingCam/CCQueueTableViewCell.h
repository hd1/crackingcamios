//
//  CCQueueTableViewCell.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCQueueTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *placeNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *openingHoursLabel;
@property (strong, nonatomic) IBOutlet UIImageView *ratingImageView;
@property (strong, nonatomic) IBOutlet UILabel *friendsCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *getDirectionsButton;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;

@end
