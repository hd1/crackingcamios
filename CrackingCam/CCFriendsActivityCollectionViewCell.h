//
//  CCFriendsActivityCollectionViewCell.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/20/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface CCFriendsActivityCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) PFImageView *imageView;
@property (nonatomic, strong) UIImageView *ratingView;
@property (nonatomic, strong) UIImageView *videoPlayer;

@property (nonatomic, strong) UILabel *pubName;
@property (nonatomic, strong) UILabel *time;


@end
