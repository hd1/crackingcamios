//
//  CCAppDelegate.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
