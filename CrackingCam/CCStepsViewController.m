//
//  CCStepsViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/19/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCStepsViewController.h"

@interface CCStepsViewController ()

@end

@implementation CCStepsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableAttributedString *postingText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Posting a score is simple!"]];
    [postingText addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(19,7)];
    [self.postingLabel setAttributedText:postingText];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationItem.hidesBackButton = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)letsGetStarted:(id)sender {
    [self performSegueWithIdentifier:@"Steps_Rate" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
