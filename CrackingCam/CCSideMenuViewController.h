//
//  CCSideMenuViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/13/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCParentViewController.h"

@interface CCSideMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
