//
//  CCGooglePlace.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/6/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCGooglePlace.h"

@interface CCGooglePlace()
@end

@implementation CCGooglePlace

@synthesize name, identifier, location, reference;


+ (CCGooglePlace *)placeFromDictionary:(NSDictionary *)placeDictionary {
    CCGooglePlace *place = [[self alloc] init];
    place.name = [[[placeDictionary objectForKey:@"name"]componentsSeparatedByString:@","]objectAtIndex:0];
    place.identifier = [placeDictionary objectForKey:@"id"];
    NSDictionary *locationDictionary = [[placeDictionary objectForKey:@"geometry"] objectForKey:@"location"];
    place.location = CLLocationCoordinate2DMake([[locationDictionary objectForKey:@"lat"] doubleValue],[[locationDictionary objectForKey:@"lng"] doubleValue]);
    place.reference = [placeDictionary objectForKey:@"reference"];
    place.vicinity = [[[placeDictionary objectForKey:@"vicinity"]componentsSeparatedByString:@","]lastObject];
    return place;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Name: %@, Location: %f %f, Identifier: %@",
            name, location.latitude,location.longitude, identifier];
}

@end
