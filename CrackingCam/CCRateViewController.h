//
//  CCRateViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/16/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCParentViewController.h"

@interface CCRateViewController : CCParentViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *overlay;

@property BOOL isAlreadyInQueue;
@property (atomic) NSInteger reviewsCount;
@property (atomic) NSInteger friendsCount;

@end
