//
//  CCCustomInfoWindow.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/7/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCCustomInfoWindow : UIView
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UILabel *timingsLabel;
@property (strong, nonatomic) IBOutlet UILabel *friendsCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *addToQueueButton;
@property (strong, nonatomic) IBOutlet UIButton *showDetailsButton;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@end
