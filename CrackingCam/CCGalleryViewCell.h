//
//  CCGalleryViewCell.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/18/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface CCGalleryViewCell : UICollectionViewCell
@property (nonatomic, strong) PFImageView *imageView;
@property (nonatomic, strong) UIImageView *ratingImage;
@property (nonatomic, strong) UIImageView *videoPlayer;
@property (nonatomic, strong) UIImageView *userProfilePic;
@property (nonatomic, strong) UIView *userBG;

@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *ratingLabel;
@property (nonatomic, strong) UILabel *captionLabel;
@property (nonatomic, strong) UILabel *time;

@property (nonatomic, strong) UIButton *closeButton;

@end
