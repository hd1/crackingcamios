//
//  CCAboutUsViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCAboutUsViewController.h"

@interface CCAboutUsViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *aboutUsWebView;
@end

@implementation CCAboutUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"About-BG.png"]]];
    [self.aboutUsWebView setBackgroundColor:[UIColor clearColor]];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutUsHTML" ofType:@"html"];
    NSURL *url=[NSURL fileURLWithPath:htmlFile];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    [self.aboutUsWebView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (IBAction)email:(id)sender {
    NSString *emailTitle = @"CrackingCam Help";
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"help@crackingcam.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setToRecipients:toRecipents];
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *mailAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"Email didn't succeed. Please try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [mailAlert show];
        }
        break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NO];
}

- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
