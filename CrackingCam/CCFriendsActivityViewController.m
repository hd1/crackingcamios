//
//  CCFriendsActivityViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/21/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <Parse/Parse.h>

#import "CCFriendsActivityViewController.h"
#import "SWRevealViewController/SWRevealViewController.h"
#import "CCGalleryViewController.h"
#import "CCFriendsActivityCollectionViewCell.h"

@interface CCFriendsActivityViewController ()
{
    SWRevealViewController *revealVC;
    NSIndexPath *selectedIndexPath;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation CCFriendsActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.topItem.title = @"";

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];

    PFObject *userLocationObject = [self.media objectAtIndex:0];
    PFUser *user = [userLocationObject objectForKey:@"user"];

    UIView *navigationTitleView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 40.0, 270.0, 44.0)];
    
    UIImageView *userPic = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 34.0, 34.0)];
    [userPic setImageWithURL:[NSURL URLWithString:[user objectForKey:@"fbProfilePicURL"]]  placeholderImage:nil options:SDWebImageProgressiveDownload];
    userPic.layer.cornerRadius = 5.0;
    userPic.layer.masksToBounds = YES;
    userPic.layer.borderWidth = 1.0;
    userPic.layer.borderColor   = [UIColor whiteColor].CGColor;
    [navigationTitleView addSubview:userPic];
    
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(35.0,0.0,250,44.0)];
    navTitle.textAlignment = NSTextAlignmentLeft;
    [navTitle setText:[NSString stringWithFormat:@"  %@ Activities",[user valueForKey:@"fbUsername"]]];
    navTitle.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    navTitle.textColor = [UIColor whiteColor];
    [navigationTitleView addSubview:navTitle];
    
    self.navigationItem.titleView = navigationTitleView;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    [self.collectionView registerClass:[CCFriendsActivityCollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(155.5, 139.0);
    layout.minimumLineSpacing = 3;
    layout.minimumInteritemSpacing = 3;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView.collectionViewLayout = layout;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - UICollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.media count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"identifier";
    
    CCFriendsActivityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    PFObject *userLocationObject = [self.media objectAtIndex:indexPath.row];
    NSDate *updatedDate = [userLocationObject updatedAt];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:updatedDate];
    NSInteger minutes = timeInterval/60.0;
    if (minutes < 60) {
        [cell.time setText:[NSString stringWithFormat:@"%ld min ago",(long)minutes]];
    }
    else
    {
        [cell.time setText:[NSString stringWithFormat:@"%ld hr ago",(long)minutes/60]];
    }
    
    NSString *locationName = [userLocationObject objectForKey:@"locationName"];
    [cell.pubName setText:locationName];
    
    PFFile *dataFile = [userLocationObject objectForKey:@"dataFile"];
    if ([[userLocationObject valueForKey:@"isVideo"]boolValue]) {
        cell.videoPlayer.hidden = NO;
        PFFile *thumbnailFile = [userLocationObject objectForKey:@"videoThumbnail"];
        [cell.imageView setFile:thumbnailFile];
        [cell.imageView loadInBackground];
    }
    else
    {
        cell.videoPlayer.hidden = YES;
        [dataFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            cell.imageView.image = [UIImage imageWithData:data];
        }];
    }
    NSInteger score = [[userLocationObject valueForKey:@"score"]integerValue];
    switch (score) {
        case 1:
            cell.ratingView.image = [UIImage imageNamed:@"Crap-PubRating.png"];
            break;
        case 2:
            cell.ratingView.image = [UIImage imageNamed:@"Boring-PubRating.png"];
            break;
        case 3:
            cell.ratingView.image = [UIImage imageNamed:@"Better-PubRating.png"];
            break;
        case 4:
            cell.ratingView.image = [UIImage imageNamed:@"Cracking-PubRating.png"];
            break;
            
        default:
            break;
    }

    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"FriendsActivity_Gallery" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FriendsActivity_Gallery"]) {
        CCGalleryViewController *galleryVC = [segue destinationViewController];
        galleryVC.media = self.media;
        galleryVC.isFromFriendsActivity = YES;
        galleryVC.indexPath = selectedIndexPath;
    }
}


@end
