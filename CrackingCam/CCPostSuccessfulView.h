//
//  CCPostSuccessfulView.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/19/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCPostSuccessfulView : UIView
@property (strong, nonatomic) IBOutlet UIView *timeBGView;
@property (strong, nonatomic) IBOutlet UILabel *remainingPostsLabel;

@end
