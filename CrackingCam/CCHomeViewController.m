//
//  CCHomeViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/1/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCHomeViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

#import "CCGooglePlacesPlacesSearchQuery.h"
#import "CCGooglePlace.h"
#import "CCCustomInfoWindow.h"
#import "CCGooglePlacesPlaceDetailQuery.h"
#import "CCGlobalData.h"
#import "CCPubRatingViewController.h"
#import "CCFriendsListViewController.h"
#import "CCRateViewController.h"
#import "CCGooglePlacesAutocompleteQuery.h"
#import "CCGooglePlacesAutocompletePlace.h"

@interface CCHomeViewController ()
{
    SWRevealViewController *revealVC;
    CCCustomInfoWindow *infoWindow;
    NSDateFormatter *dateFormatter;
    BOOL isGesture;
    NSArray *friends;
    int friendsCount;
    NSInteger reviewsCount;
    NSInteger rating;
    BOOL isAlreadyInQueue;
    NSMutableArray *friendsAtCurrentPub;
    NSMutableArray *currentMarkedPlaces;
    BOOL shouldPerformSearchAndMarkPlaces;
    BOOL isSearchAndMarkPlacesPerformed;
    
    CLLocationManager *locationManager;
    
    CCGooglePlacesAutocompleteQuery *autoCompleteQuery;
    BOOL canPerformSearchAndMarkPlaces;
}

@property (strong, nonatomic) IBOutlet UIButton *friendsHereButton;
@end

@implementation CCHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    canPerformSearchAndMarkPlaces = NO;
    isSearchAndMarkPlacesPerformed = NO;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.activityType    = CLActivityTypeAutomotiveNavigation;
    [locationManager startUpdatingLocation];
    
    currentMarkedPlaces = [[NSMutableArray alloc]init];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];

    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"c"];
    int currentDay = [[dateFormatter stringFromDate:[NSDate date]]intValue] - 1;
    [CCGlobalData sharedGlobalData].currentDay = [NSString stringWithFormat:@"%d",currentDay];
    

    placesSearchQuery = [[CCGooglePlacesPlacesSearchQuery alloc]init];
    placesSearchQuery.radius = 3000.0;
    
    autoCompleteQuery = [[CCGooglePlacesAutocompleteQuery alloc]init];
    autoCompleteQuery.radius = 50000.0;
    
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor whiteColor]];
    
     self.searchDisplayController.searchBar.placeholder = @"Search for location, name or anything”";
    [self.searchDisplayController.searchBar setImage:[UIImage imageNamed:@"Search-Icon-unselected.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.searchDisplayController.searchBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Sticky-Footer-BG.png"]]];
    [self.searchDisplayController.searchBar setTintColor:[UIColor grayColor]];
        
    [self setupMapView];
    [self findFriends];
    [infoWindow.showDetailsButton addTarget:self action:@selector(showDetails:) forControlEvents:UIControlEventTouchUpInside];
    if ([CCGlobalData sharedGlobalData].remoteNotificationLocationId) {
        [self performSegueWithIdentifier:@"Home_PubRating" sender:self];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self removeInfoWindow];
    self.navigationController.navigationBarHidden = NO;
    [self setupNavigationBar];
    [self setupSideMenuButton];
    [currentMarkedPlaces removeAllObjects];
    [_mapView clear];
    [locationManager startUpdatingLocation];
    shouldPerformSearchAndMarkPlaces = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)searchLocations
{
    self.searchDisplayController.searchBar.hidden = NO;
    [self.searchDisplayController.searchBar becomeFirstResponder];
}
- (void)findInAppFriends
{
    [self showActivityIndicator];
    PFQuery *friendQuery = [PFUser query];
    [friendQuery whereKey:@"fbId" containedIn:[CCGlobalData sharedGlobalData].fbFriendsIds];
    friendQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
    friendQuery.maxCacheAge = 5 * 60;
    [friendQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(!error)
        {
            friends = [NSArray arrayWithArray:objects];
            if ([[CCGlobalData sharedGlobalData].friends count] > 0) {
                [[CCGlobalData sharedGlobalData].friends removeAllObjects];
            }
            [[CCGlobalData sharedGlobalData].friends addObjectsFromArray:objects];
            canPerformSearchAndMarkPlaces = YES;
            if (!isSearchAndMarkPlacesPerformed) {
                placesSearchQuery.location = _mapView.myLocation.coordinate;
                placesSearchQuery.name     = nil;
                [self searchAndMarkPlaces];
            }
        }
        else{
            [self showErrorAlertView:error.localizedDescription];
        }
    }];
}

- (void)findFriends
{
    [self showActivityIndicator];
    if ([[CCGlobalData sharedGlobalData].fbFriendsIds count] ==0 ) {
        [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                NSArray *friendObjects = [result objectForKey:@"data"];
                for (NSDictionary *friendObject in friendObjects) {
                    [[CCGlobalData sharedGlobalData].fbFriendsIds addObject:[friendObject objectForKey:@"id"]];
                }
                [self findInAppFriends];
            }
            else
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"logoutUser" object:nil];
            }
            [self hideActivityIndicator];
        }];
    }
    else
    {
        [self findInAppFriends];
    }
}

- (void)setupNavigationBar
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchLocations)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton, nil];
}
- (void)setupMapView
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                                            longitude:locationManager.location.coordinate.longitude
                                                                 zoom:14];
    _mapView = [GMSMapView mapWithFrame:CGRectMake(self.searchDisplayController.searchBar.frame.origin.x, self.searchDisplayController.searchBar.frame.origin.y + self.searchDisplayController.searchBar.frame.size.height,self.searchDisplayController.searchBar.frame.size.width, self.view.frame.size.height - self.searchDisplayController.searchBar.frame.size.height-20.0) camera:camera];
    _mapView.delegate = self;
    _mapView.myLocationEnabled = YES;
    _mapView.settings.myLocationButton = YES;
   
    infoWindow = [[[NSBundle mainBundle]loadNibNamed:@"InfoWindow" owner:self options:nil]objectAtIndex:0];
    [self.view addSubview:_mapView];
}

- (void)setupSideMenuButton
{
    revealVC = [self revealViewController];
    revealVC.delegate = self;
    [revealVC tapGestureRecognizer];
    UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-Friends"] style:UIBarButtonItemStylePlain target:revealVC action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = sideMenuButton;
    self.navigationItem.hidesBackButton = YES;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    switch (position) {
        case FrontViewPositionRight:
           [_mapView.settings setAllGesturesEnabled:NO];
            break;
        case FrontViewPositionLeft:
            [_mapView.settings setAllGesturesEnabled:YES];
            break;

        default:
            break;
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [searchResultPlaces count];
}

- (CCGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return [searchResultPlaces objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CCGooglePlacesAutocompleteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
   
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    CCGooglePlacesPlaceDetailQuery *query = [CCGooglePlacesPlaceDetailQuery query];
    query.reference = place.reference;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
        if (error) {
        } else {
            NSArray *types = [placeDictionary objectForKey:@"types"];
            NSArray *allowedTypes = @[@"bar",@"night_club",@"casino",@"stadium"];
            
            for (NSString *type in types) {
                if ([allowedTypes containsObject:type]) {
                    CCGooglePlace *googlePlace = [[CCGooglePlace alloc]init];
                    googlePlace.name = [[place.description componentsSeparatedByString:@","]firstObject];
                    googlePlace.identifier = place.identifier;
                    googlePlace.reference = place.reference;
                    googlePlace.vicinity = [[[placeDictionary objectForKey:@"vicinity"]componentsSeparatedByString:@","]lastObject];
                    NSDictionary *locationDic = [[placeDictionary objectForKey:@"geometry"]objectForKey:@"location"];
                    googlePlace.location = CLLocationCoordinate2DMake([[locationDic valueForKey:@"lat"]doubleValue], [[locationDic valueForKey:@"lng"]doubleValue]);
                    [_mapView clear];
                    [[NSOperationQueue mainQueue]cancelAllOperations];
                    [currentMarkedPlaces removeAllObjects];
                    [self markPlace:googlePlace];
                    [_mapView animateToLocation:googlePlace.location];
                    [self.searchDisplayController setActive:NO];
                    [self.searchDisplayController.searchBar resignFirstResponder];
                    return;
                }
            }
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"This location is not supported by CrackingCam. Please try another location." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            [self.searchDisplayController setActive:NO];
            [self.searchDisplayController.searchBar resignFirstResponder];
        }
    }];
}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    [self showActivityIndicator];
    autoCompleteQuery.location = _mapView.myLocation.coordinate;
    autoCompleteQuery.input = searchString;
    [autoCompleteQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            searchResultPlaces = places;
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
        [self hideActivityIndicator];
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self removeInfoWindow];
    [self handleSearchForSearchString:searchString];
    return YES;
}

#pragma mark - UISearchBar Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    tableView.frame = CGRectMake(self.searchDisplayController.searchBar.frame.origin.x, self.searchDisplayController.searchBar.frame.origin.y-20.0,self.searchDisplayController.searchBar.frame.size.width, 176.0);
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    self.searchDisplayController.searchBar.hidden = YES;
    [self.searchDisplayController setActive:NO];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [self.searchDisplayController setActive:NO];
}

#pragma mark - GMSMapViewDelegate Methods

- (BOOL) didTapMyLocationButtonForMapView:(GMSMapView *)mapView
{
    [self removeInfoWindow];
    [_mapView clear];
    [currentMarkedPlaces removeAllObjects];
    placesSearchQuery.name     = nil;
    placesSearchQuery.location = _mapView.myLocation.coordinate;
    [self searchAndMarkPlaces];
    [_mapView animateToZoom:14.0];
    return NO;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    return NO;
}

- (void)setInfoWindowTimingsLabel:(NSDictionary *)timings
{
    if ([timings objectForKey:[CCGlobalData sharedGlobalData].currentDay] != nil) {
        [infoWindow.timingsLabel setText:[timings objectForKey:[CCGlobalData sharedGlobalData].currentDay]];
    }
    else
    {
        [infoWindow.timingsLabel setText:@"Not available"];
    }
    
}

- (NSDictionary *)processLocationTimings:(NSArray *)periods
{
    NSMutableDictionary *timingsDic = [[NSMutableDictionary alloc]init];
    for (int i = 0; i < [periods count]; i++) {
        NSDictionary *dic = [periods objectAtIndex:i];
        NSString *open,*close,*timing;
        if ([dic objectForKey:@"open"] != nil) {
            NSString *open24 = [NSString stringWithFormat:@"%@:%@",[[[dic objectForKey:@"open"] objectForKey:@"time"] substringToIndex:2],[[[dic objectForKey:@"open"] objectForKey:@"time"] substringFromIndex:2]];
            open = [self get12HourFormat:open24];
        }
        else
        {
            open = @"NA";
        }
        if ([dic objectForKey:@"close"] != nil) {
           NSString *close24 = [NSString stringWithFormat:@"%@:%@",[[[dic objectForKey:@"close"] objectForKey:@"time"] substringToIndex:2],[[[dic objectForKey:@"close"] objectForKey:@"time"] substringFromIndex:2]];
            close = [self get12HourFormat:close24];
        }
        else
        {
            close = @"NA";
        }
        timing = [NSString stringWithFormat:@"%@ to %@",open,close];
        [timingsDic setObject:timing forKey:[NSString stringWithFormat:@"%@",[[dic objectForKey:@"open"] objectForKey:@"day"]]];
    }
    return timingsDic;
}

- (NSString *)get12HourFormat:(NSString *)time
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    NSDate *date = [formatter dateFromString:time];
    
    formatter.dateFormat = @"hh:mm a";
    NSString *pmamDateString = [formatter stringFromDate:date];
    
    return pmamDateString;
}
- (NSDictionary *)fetchLocationTimings:(NSString *)locationReference
{
    CCGooglePlacesPlaceDetailQuery *query = [CCGooglePlacesPlaceDetailQuery query];
    query.reference = locationReference;
    __block NSDictionary *timings;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
        if (error) {
        } else {
            NSArray *periods = [[placeDictionary objectForKey:@"opening_hours"]objectForKey:@"periods"];
            if (periods != nil) {
                timings = [self processLocationTimings:periods];
                [self setInfoWindowTimingsLabel:timings];
            }
            else [infoWindow.timingsLabel setText:@"Not available"];
        }
    }];
    return timings;
}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    [self removeInfoWindow];
    rating = [[marker.userData objectForKey:@"score"] integerValue];
    __block CCGooglePlace *place = [marker.userData objectForKey:@"googlePlace"];
    isAlreadyInQueue = NO;
        [self showActivityIndicator];
        PFQuery *queueQuery = [PFQuery queryWithClassName:@"UserQueue"];
        [queueQuery whereKey:@"locationId" equalTo:place.identifier];
        [queueQuery whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
        [queueQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
         {
             if(!error){
                 if (object != nil) {
                     NSDictionary *timings = [[object objectForKey:@"locationDetails"]objectForKey:@"timings"];
                     if (timings != nil) {
                         place.timings = timings;
                         [self setInfoWindowTimingsLabel:timings];
                     }
                     else
                     {
                         CCGooglePlacesPlaceDetailQuery *query = [CCGooglePlacesPlaceDetailQuery query];
                         query.reference = place.reference;
                         [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
                             if (error) {
                             } else {
                                 NSArray *periods = [[placeDictionary objectForKey:@"opening_hours"]objectForKey:@"periods"];
                                 if (periods != nil) {
                                     NSDictionary *timings = [self processLocationTimings:periods];
                                     place.timings = timings;
                                     [self setInfoWindowTimingsLabel:timings];
                                 }
                                 else [infoWindow.timingsLabel setText:@"Not available"];
                             }
                         }];

                     }
                     isAlreadyInQueue = YES;
                     [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = YES;
                     [infoWindow.addToQueueButton setImage:[UIImage imageNamed:@"Already-in-Queue.png"] forState:UIControlStateNormal];
                     infoWindow.addToQueueButton.enabled = NO;
                     [infoWindow reloadInputViews];
                 }
                 else
                 {
                     infoWindow.addToQueueButton.enabled = YES;
                 }
             }
             else
             {
                 infoWindow.addToQueueButton.enabled = YES;
                 if(![[error.userInfo objectForKey:@"error"] isEqualToString:@"no results matched the query"])
                 {
                     [self showErrorAlertView:error.localizedDescription];
                 }
                 else
                 {
                     CCGooglePlacesPlaceDetailQuery *query = [CCGooglePlacesPlaceDetailQuery query];
                     query.reference = place.reference;
                     [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
                         if (error) {
                         } else {
                             NSArray *periods = [[placeDictionary objectForKey:@"opening_hours"]objectForKey:@"periods"];
                             if (periods != nil) {
                                 NSDictionary *timings = [self processLocationTimings:periods];
                                 place.timings = timings;
                                 [self setInfoWindowTimingsLabel:timings];
                             }
                             else [infoWindow.timingsLabel setText:@"Not available"];
                         }
                     }];
                 }
             }
             [self hideActivityIndicator];
         }];
    
        [CCGlobalData sharedGlobalData].selectedPlace = place;
        friendsCount = [[marker.userData objectForKey:@"friendsCount"] intValue];
        friendsAtCurrentPub = [marker.userData objectForKey:@"friendsHere"];
        [self.friendsHereButton setTitle:[NSString stringWithFormat:@"%d",friendsCount] forState:UIControlStateNormal];
        
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ (%@)",place.name, place.vicinity]];
        [mutableString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:10.0] range:NSMakeRange(place.name.length, place.vicinity.length+3)];
        [infoWindow.infoLabel setAttributedText:mutableString];
        
        if ([UIScreen mainScreen].bounds.size.height == 568.0) {
            infoWindow.center = CGPointMake(160, 195.0);
        }
        else
        {
            infoWindow.center = CGPointMake(160, 165);
        }
    
    [self showActivityIndicator];
    if (rating != 0) {
        infoWindow.showDetailsButton.tag = 1;
        [infoWindow.showDetailsButton setImage:[UIImage imageNamed:@"Orange-arrow.png"] forState:UIControlStateNormal];
        PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
        [locationsQuery whereKey:@"locationId" equalTo:place.identifier];
        PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
        [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
        [mediaQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error)
         {
             if(!error)
             {
                 [infoWindow.reviewsCountLabel setText:[NSString stringWithFormat:@"%d",number]];
                 reviewsCount = number;
             }
             else if([[error.userInfo objectForKey:@"error"] isEqualToString:@"no results matched the query"])
             {
                 [infoWindow.reviewsCountLabel setText:@"0"];
             }
             else
             {
                 [self showErrorAlertView:error.localizedDescription];
             }
             [CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace = friendsCount;
             [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace = reviewsCount;
             [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = isAlreadyInQueue;
             [self.view addSubview:infoWindow];
             [self hideActivityIndicator];
         }];
    }
    else
    {
        [CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace = friendsCount;
        [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace = reviewsCount;
        [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = isAlreadyInQueue;
        [self.view addSubview:infoWindow];
        [self hideActivityIndicator];
        infoWindow.showDetailsButton.tag = 2;
        [infoWindow.showDetailsButton setImage:[UIImage imageNamed:@"camera-Icon.png"] forState:UIControlStateNormal];
    }
    
    return nil;
}

- (void)addRating
{
    CLLocation *selectedPlaceLocation = [[CLLocation alloc]initWithLatitude:[CCGlobalData sharedGlobalData].selectedPlace.location.latitude longitude:[CCGlobalData sharedGlobalData].selectedPlace.location.longitude];
    CLLocationDistance distance = [selectedPlaceLocation distanceFromLocation:_mapView.myLocation];
    if (distance <= 1609.344) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSInteger count = [userDefaults integerForKey:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
        if (count == 1) {
            [[[UIAlertView alloc]initWithTitle:nil message:@"You have posted maximum allowed times per day" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }
        else
        {
            NSDate *date = [userDefaults objectForKey:@"lastPublishedDate"];
            if (date != nil) {
                NSTimeInterval timeIntervalSinceLastPost = [[NSDate date]timeIntervalSinceDate:date];
                NSInteger minutes = timeIntervalSinceLastPost/60.0;
                if (minutes >= 15) {
                    if (minutes > 1440) {
                        NSString *appDomain = [[NSBundle mainBundle]bundleIdentifier];
                        [userDefaults removePersistentDomainForName:appDomain];
                    }
                    [self performSegueWithIdentifier:@"Home_Rate" sender:self];
                }
                else
                {
                    [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%ld min left for your next post",15 - (long)minutes] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                }
            }
            else
            {
                [self performSegueWithIdentifier:@"Home_Rate" sender:self];
            }
        }
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you are at this venue?  Your current location seems a little far."] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }
    
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    [self performSegueWithIdentifier:@"HomeToReview" sender:self];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self removeInfoWindow];
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    if (gesture)
    {
        isGesture = YES;
        [self removeInfoWindow];
    }
    else
    {
        isGesture = NO;
    }
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if (isGesture) {
        placesSearchQuery.location = position.target;
        placesSearchQuery.name     = nil;
        if (position.zoom < 6.0) {
            placesSearchQuery.radius = 50000.0;
        }
        else
        {
            placesSearchQuery.radius   = 50000.0/(1.7*position.zoom);
        }
        [self searchAndMarkPlaces];
    }

}

- (void)markPlaces:(NSArray *)googlePlaces
{
    [self showActivityIndicator];
    NSArray *placesIds = [googlePlaces valueForKey:@"identifier"];
    PFQuery *scoreQuery = [PFQuery queryWithClassName:@"UserLocation"];
    [scoreQuery whereKey:@"locationId" containedIn:placesIds];
    [scoreQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    {
        if(!error){
            for (CCGooglePlace *googlePlace in googlePlaces) {
                NSMutableDictionary *userData = [[NSMutableDictionary alloc]init];
                [userData setObject:googlePlace forKey:@"googlePlace"];
                GMSMarker *marker = [GMSMarker markerWithPosition:googlePlace.location];
                NSInteger averageScore = 0;
                int countForAverageScore = 0;
                int friendCount = 0;
                NSMutableArray *friendsHere = [[NSMutableArray alloc]init];
                
                for (PFObject *userLocation in objects) {
                    if ([[userLocation valueForKey:@"locationId"] isEqualToString:googlePlace.identifier]) {
                        for (PFObject *friend in friends) {
                            if ([[friend objectId] isEqualToString:[[userLocation valueForKey:@"userObjectId"] objectId]]) {
                                friendCount++;
                                [friendsHere addObject:friend];
                            }
                        }
                        countForAverageScore++;
                        averageScore = averageScore + [[userLocation valueForKey:@"score"]integerValue];
                    }
                }
                [userData setValue:[NSString stringWithFormat:@"%d",friendCount] forKey:@"friendsCount"];
                [userData setValue:friendsHere forKey:@"friendsHere"];
                if (countForAverageScore > 0) {
                    averageScore = ceilf(averageScore/countForAverageScore);
                }
                [userData setValue:[NSNumber numberWithInteger:averageScore] forKey:@"score"];
                if (friendCount > 0) {
                    marker.icon = [self getMarkerImageForScore:averageScore andFriendsCount:friendCount];
                }
                else
                {
                    switch (averageScore) {
                        case 0:
                            marker.icon = [UIImage imageNamed:@"No-Review-Icon.png"];
                            break;
                        case 1:
                            marker.icon = [UIImage imageNamed:@"It's-Crap-Icon.png"];
                            break;
                        case 2:
                            marker.icon = [UIImage imageNamed:@"It's-Boring-Icon.png"];
                            break;
                        case 3:
                            marker.icon = [UIImage imageNamed:@"It's-Better-Icon.png"];
                            break;
                        case 4:
                            marker.icon = [UIImage imageNamed:@"It's-Cracking-Icon.png"];
                            break;
                        default:
                            break;
                    }
                    
                }
                marker.userData = userData;
                marker.map = _mapView;
                [currentMarkedPlaces addObject:googlePlace.identifier];
            }
        }
        else
        {
            [self showErrorAlertView:error.localizedDescription];
        }
        [self hideActivityIndicator];
    }];
}

- (void)markPlace:(CCGooglePlace *)googlePlace
{
    NSMutableDictionary *userData = [[NSMutableDictionary alloc]init];
    [userData setObject:googlePlace forKey:@"googlePlace"];
    
         GMSMarker *marker = [GMSMarker markerWithPosition:googlePlace.location];
         PFQuery *scoreQuery = [PFQuery queryWithClassName:@"UserLocation"];
    
         [scoreQuery whereKey:@"locationId" equalTo:googlePlace.identifier];
         [scoreQuery selectKeys:[NSArray arrayWithObjects:@"userObjectId",@"score",nil]];
         [scoreQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
          {
            if(!error){
              NSInteger averageScore = 0;
              int friendCount = 0;
              if ([objects count] > 0) {
                  NSMutableArray *friendsHere = [[NSMutableArray alloc]init];
                  for (PFObject *object in objects) {
                      for (PFObject *friend in friends) {
                          if ([[friend objectId] isEqualToString:[[object valueForKey:@"userObjectId"] objectId]]) {
                              friendCount++;
                              [friendsHere addObject:friend];
                          }
                      }
                      averageScore = averageScore + [[object valueForKey:@"score"]integerValue];
                  }
                  [userData setValue:[NSString stringWithFormat:@"%d",friendCount] forKey:@"friendsCount"];
                  [userData setValue:friendsHere forKey:@"friendsHere"];
                  averageScore = averageScore/[objects count];
                  if (friendCount > 0) {
                      marker.icon = [self getMarkerImageForScore:averageScore andFriendsCount:friendCount];
                  }
                  else
                  {
                      switch (averageScore) {
                          case 1:
                              marker.icon = [UIImage imageNamed:@"It's-Crap-Icon.png"];
                              break;
                          case 2:
                              marker.icon = [UIImage imageNamed:@"It's-Boring-Icon.png"];
                              break;
                          case 3:
                              marker.icon = [UIImage imageNamed:@"It's-Better-Icon.png"];
                              break;
                          case 4:
                              marker.icon = [UIImage imageNamed:@"It's-Cracking-Icon.png"];
                              break;
                          default:
                              break;
                      }
                  }
                }
              else
              {
                  marker.icon = [UIImage imageNamed:@"No-Review-Icon.png"];
              }
              [userData setValue:[NSNumber numberWithInteger:averageScore] forKey:@"score"];
              marker.userData = userData;
              marker.map = _mapView;
                [currentMarkedPlaces addObject:googlePlace.identifier];
              }
              else
              {
                  [self showErrorAlertView:error.localizedDescription];
              }
              [self hideActivityIndicator];
          }];
}

- (IBAction)friendsHere:(id)sender {
    if ([friendsAtCurrentPub count] > 0) {
        [self performSegueWithIdentifier:@"Home_Friends" sender:self];
    }
}
- (void)searchAndMarkPlaces
{
    [self showActivityIndicator];
    [[NSOperationQueue mainQueue]cancelAllOperations];
    [placesSearchQuery fetchPlaces:^(NSArray *places, NSError *error){
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            NSMutableArray *placesToMark = [[NSMutableArray alloc]init];
            for (CCGooglePlace *googlePlace in places) {
                if (![currentMarkedPlaces containsObject:googlePlace.identifier]) {
                    [placesToMark addObject:googlePlace];
                }
            }
            if ([placesToMark count] > 0) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self markPlaces:placesToMark];
                }];
            }
        }
        [self hideActivityIndicator];
    }];
}

- (UIImage *)getMarkerImageForScore:(NSInteger)score andFriendsCount:(int)friendCount
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 65)];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *circle = [[UIImageView alloc]initWithFrame:CGRectMake(4, 4, 24, 24)];
    UIImageView *markerImage = [[UIImageView alloc]initWithFrame:CGRectMake(13, 7, 40, 48)];
    switch (score) {
        case 1:
            markerImage.image = [UIImage imageNamed:@"It's-Crap-Icon.png"];
            circle.image      = [UIImage imageNamed:@"Red-Circle.png"];
            break;
        case 2:
            markerImage.image = [UIImage imageNamed:@"It's-Boring-Icon.png"];
            circle.image      = [UIImage imageNamed:@"Yellow-Circle.png"];
            break;
        case 3:
            markerImage.image = [UIImage imageNamed:@"It's-Better-Icon.png"];
            circle.image      = [UIImage imageNamed:@"Blue-Circle.png"];
            break;
        case 4:
            markerImage.image = [UIImage imageNamed:@"It's-Cracking-Icon.png"];
            circle.image      = [UIImage imageNamed:@"Green-Circle.png"];
            break;
        default:
            break;
    }
    [view addSubview:markerImage];
    [view addSubview:circle];
    UILabel *scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 22, 22)];
    [scoreLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.0]];
    [scoreLabel setTextAlignment:NSTextAlignmentCenter];
    [scoreLabel setText:[NSString stringWithFormat:@"%d",friendCount]];
    [view addSubview:scoreLabel];
    CGRect rect = view.frame;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)removeInfoWindow
{
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[CCCustomInfoWindow class]]) {
            [subView removeFromSuperview];
        }
    }
    [infoWindow.reviewsCountLabel setText:@"0"];
    [infoWindow.addToQueueButton setImage:[UIImage imageNamed:@"Add-To-Queue.png"] forState:UIControlStateNormal];
    infoWindow.addToQueueButton.enabled = NO;
}
- (void)showDetails:(UIButton *)sender
{
    if (sender.tag == 1) {
        [self performSegueWithIdentifier:@"Home_PubRating" sender:self];
    }
    else
    {
        [self addRating];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Home_Friends"])
    {
        CCFriendsListViewController *friendsListVC = [segue destinationViewController];
        friendsListVC.isFromPub = YES;
        friendsListVC.friendsAtCurrentPub = [[NSMutableArray alloc]initWithArray:friendsAtCurrentPub];
    }
    else if ([segue.identifier isEqualToString:@"Home_Rate"])
    {
        CCRateViewController *rateVC = [segue destinationViewController];
        rateVC.isAlreadyInQueue      = isAlreadyInQueue;
        rateVC.friendsCount          = 0;
        rateVC.reviewsCount          = 0;
    }
}

- (IBAction)addToQueue:(id)sender
{
    [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = YES;
    isAlreadyInQueue = YES;
    [infoWindow.addToQueueButton setImage:[UIImage imageNamed:@"Added-to-Queue.png"] forState:UIControlStateNormal];
    [infoWindow.addToQueueButton setEnabled:NO];

    CCGooglePlace *place = [CCGlobalData sharedGlobalData].selectedPlace;
    PFObject *queue = [PFObject objectWithClassName:@"UserQueue"];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setObject:place.name  forKey:@"placeName"];
    [dictionary setObject:place.vicinity forKey:@"vicinity"];
    if (place.timings != nil) {
        [dictionary setObject:place.timings forKey:@"timings"];
    }
    [queue setObject:dictionary  forKey:@"locationDetails"];
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:place.location.latitude longitude:place.location.longitude];
    [queue setObject:geoPoint forKey:@"location"];
    [queue setObject:[PFUser currentUser] forKey:@"userObjectId"];
    [queue setObject:place.identifier forKey:@"locationId"];
    [queue saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (!succeeded) {
             isAlreadyInQueue = NO;
             [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = NO;
             [infoWindow.addToQueueButton setImage:[UIImage imageNamed:@"Add-To-Queue.png"] forState:UIControlStateNormal];
             [infoWindow.addToQueueButton setEnabled:YES];
             [self showErrorAlertView:@"Unable to add to queue. Please try again after sometime"];
         }
     }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (shouldPerformSearchAndMarkPlaces) {
        shouldPerformSearchAndMarkPlaces = NO;
        CLLocation *location = [locations lastObject];
        placesSearchQuery.location = location.coordinate;
        placesSearchQuery.name     = nil;
        if (canPerformSearchAndMarkPlaces) {
            isSearchAndMarkPlacesPerformed = YES;
            [self searchAndMarkPlaces];
        }
        GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:location.coordinate zoom:14.0];
        [_mapView moveCamera:cameraUpdate];
        [locationManager stopUpdatingLocation];
    }
}
@end
