//
//  CCRateViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/16/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Parse/Parse.h>

#import "CCRateViewController.h"
#import "CCGlobalData.h"
#import "CCHomeViewController.h"
#import "CCPostSuccessfulView.h"
#import "CCPubRatingViewController.h"
#import "SWRevealViewController.h"

@interface CCRateViewController ()
{
    UIImage *imageToSave;
    NSURL *videoURL;
    BOOL isPhotoMode;
    BOOL isCapturingVideo;
    BOOL isFlashModeOn;
    NSString *caption;
    int rating;
    BOOL isPrivate;
    NSData *imageData;
    SWRevealViewController *revealVC;
    NSTimer *videoTimer;
    int currentVideoTime;
    int updatedAverageRating;
    int remainingPostsCount;
}

@property (strong, nonatomic) IBOutlet UIImageView *capturedImage;
@property (nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) IBOutlet UIView *addACaptionView;
@property (strong, nonatomic) IBOutlet UITextView *captionTextView;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIView *captionViewBG;
@property (strong, nonatomic) IBOutlet UILabel *stepTwoLabel;
@property (strong, nonatomic) IBOutlet UILabel *step3Label;
@property (strong, nonatomic) IBOutlet UIImageView *stepTwoFinishedImage;
@property (strong, nonatomic) IBOutlet UIView *stepThreeView;
@property (strong, nonatomic) IBOutlet UIImageView *stepThreeFinishedImage;
@property (strong, nonatomic) IBOutlet UIView *ratingPreviewBG;
@property (strong, nonatomic) IBOutlet UIImageView *ratingImageView;
@property (strong, nonatomic) IBOutlet UILabel *captionLabel;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UIView *publishAndPrivacyBG;
@property (strong, nonatomic) IBOutlet UIView *privacyBG;
@property (strong, nonatomic) IBOutlet UIButton *privateButton;
@property (strong, nonatomic) IBOutlet UIButton *publicButton;
@property (strong, nonatomic) IBOutlet UIView *ratingPreviewView;
@property (strong, nonatomic) IBOutlet UIButton *publishButton;
@property (strong, nonatomic) IBOutlet UIImageView *ratingStrip;
@property (strong, nonatomic) IBOutlet UIButton *cameraModeButton;
@property (strong, nonatomic) IBOutlet UIButton *videoModeButton;
@property (strong, nonatomic) IBOutlet UIButton *flashButton;
@property (strong, nonatomic) IBOutlet UIButton *cameraSwitcherButton;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet CCPostSuccessfulView *postSuccessfulView;
@property (strong, nonatomic) IBOutlet UIView *postSuccessfulSubView;
@property (strong, nonatomic) IBOutlet UILabel *videoTimerLabel;
@property (strong, nonatomic) IBOutlet UIButton *captureButton;
@property (strong, nonatomic) IBOutlet UILabel *charactersRemainingLabel;

- (IBAction)publish:(id)sender;
- (IBAction)setPrivate:(id)sender;
- (IBAction)setPublic:(id)sender;



- (IBAction)capture:(id)sender;
- (IBAction)video:(id)sender;
- (IBAction)photo:(id)sender;
- (IBAction)flashControl:(id)sender;
- (IBAction)changeCamera:(id)sender;
- (IBAction)close:(id)sender;
- (IBAction)proceedAndAddScore:(id)sender;
- (IBAction)dismiss:(id)sender;
- (IBAction)doneButton:(id)sender;

@end

@implementation CCRateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initializations
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isPhotoMode = YES;
    [[NSBundle mainBundle] loadNibNamed:@"CCAddCaptionView" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"CCPostSuccessfulView" owner:self options:nil];
    
    self.addACaptionView.center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
    self.stepThreeView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];
    [self.capturedImage setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]]];
    self.stepThreeFinishedImage.hidden = YES;
    
    [self.privacyBG.layer setCornerRadius:5.0];
    [self.publicButton.layer setCornerRadius:5.0];
    [self.privateButton.layer setCornerRadius:5.0];
    [self.publishButton.layer setCornerRadius:5.0];
    [self.privateButton setBackgroundColor:[UIColor whiteColor]];
    [self.publicButton setBackgroundColor:[UIColor orangeColor]];
    
    [self.publishAndPrivacyBG setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Sticky-Footer-BG.png"]]];
    [self.ratingPreviewBG setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.7]];
    
    [self.postSuccessfulView.timeBGView.layer setCornerRadius:5.0];
    [self.postSuccessfulView.timeBGView.layer setMasksToBounds:YES];
    
    [self.postSuccessfulSubView.layer setCornerRadius:5.0];
    [self.postSuccessfulSubView.layer setMasksToBounds:YES];
    
    [self.postSuccessfulView setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.7]];
    
    rating = 4;
    [self.cameraModeButton setImage:[UIImage imageNamed:@"Camera Selected.png"] forState:UIControlStateNormal];
    [self.videoModeButton setImage:[UIImage imageNamed:@"Video Unselected.png"] forState:UIControlStateNormal];
    [self.flashButton setImage:[UIImage imageNamed:@"Flash Selected.png"] forState:UIControlStateNormal];
    [self.cameraSwitcherButton setImage:[UIImage imageNamed:@"Rotate.png"] forState:UIControlStateNormal];

    self.ratingStrip.userInteractionEnabled = YES;
    [self.doneButton setEnabled:YES];

    [self takeAPhoto];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self registerForKeyboardNotifications];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma UITextView Delegates

- (void)textViewDidEndEditing:(UITextView *)textView
{
    caption = textView.text;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self.charactersRemainingLabel setText:[NSString stringWithFormat:@"%d",50-textView.text.length]];
    if (textView.text.length >= 50) {
        [textView resignFirstResponder];
    }
}
- (BOOL) textView: (UITextView*)textView shouldChangeTextInRange: (NSRange) range replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"] ) {
        caption = textView.text;
        return NO;
    }
    return YES;
}

#pragma mark - Keyboard Handling

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.addACaptionView.center = CGPointMake(self.view.frame.size.width/2.0, (self.view.frame.size.height - kbSize.height)/2.0);
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.addACaptionView.center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
}

- (void)takeAPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        imagePickerController.showsCameraControls = NO;
        imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
        imagePickerController.videoQuality = UIImagePickerControllerQualityTypeMedium;
        imagePickerController.videoMaximumDuration = 15.0;
        imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
        
        [[NSBundle mainBundle] loadNibNamed:@"CCCameraOverlayView" owner:self options:nil];
        self.overlay.frame = imagePickerController.cameraOverlayView.frame;
        imagePickerController.cameraOverlayView = self.overlay;
        self.overlay = nil;
        
        self.imagePickerController = imagePickerController;
        [self presentViewController:self.imagePickerController animated:YES completion:nil];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([info objectForKey:@"UIImagePickerControllerMediaURL"] != nil) {
        videoURL = [info objectForKey:@"UIImagePickerControllerMediaURL"];
        UIImage *thumbnailImage = [self imageFromMovie:videoURL];
        [self.capturedImage setImage:thumbnailImage];
        imageToSave = nil;
        imageData = UIImageJPEGRepresentation(thumbnailImage, 0.0);
        [self.videoTimerLabel setHidden:YES];
        [videoTimer invalidate];
        videoTimer = nil;
    }
    else
    {
        videoURL = nil;
        UIImage *originalImage, *editedImage;
        editedImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }
        
        if ([UIScreen mainScreen].bounds.size.height == 568.0) {
            UIGraphicsBeginImageContext(CGSizeMake(640, 1136));
            [imageToSave drawInRect: CGRectMake(0, 0, 640, 1136)];
        }
        else
        {
            UIGraphicsBeginImageContext(CGSizeMake(640, 960));
            [imageToSave drawInRect: CGRectMake(0, 0, 640, 960)];
        }
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        imageData = UIImageJPEGRepresentation(smallImage, 0.0);
        [self.capturedImage setImage:imageToSave];
    }
    
    self.stepTwoFinishedImage.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
    [self.view bringSubviewToFront:self.topView];
    [self.view bringSubviewToFront:self.capturedImage];
    [self.view addSubview:self.addACaptionView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}		

- (UIImage *)imageFromMovie:(NSURL *)movieURL {
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:movieURL options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 4);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    return [[UIImage alloc] initWithCGImage:imgRef];
}

#pragma mark - camera overlay methods

- (IBAction)capture:(id)sender {
    if (isPhotoMode) {
        [self.imagePickerController takePicture];
    }
    else
    {
        if (!isCapturingVideo) {
            isCapturingVideo = YES;
            [self.imagePickerController startVideoCapture];
            currentVideoTime = 0;
            [self.videoTimerLabel setHidden:NO];
            [self.overlay bringSubviewToFront:self.videoTimerLabel];
            [self.captureButton setImage:[UIImage imageNamed:@"Capture_red.png"] forState:UIControlStateNormal];
            videoTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateSliderAndLabel) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop]addTimer:videoTimer forMode:NSRunLoopCommonModes];
        }
        else
        {
            isCapturingVideo = NO;
            [self.imagePickerController stopVideoCapture];
            [self.videoTimerLabel setHidden:YES];
            [self.captureButton setImage:[UIImage imageNamed:@"Capture_video.png"] forState:UIControlStateNormal];
            currentVideoTime = 0;
            [videoTimer invalidate];
            videoTimer = nil;
        }
    }
}

- (void)updateSliderAndLabel
{
    if (currentVideoTime > 9) {
        [self.videoTimerLabel setText:[NSString stringWithFormat:@"00:%d",currentVideoTime]];
    }
    else
    {
        [self.videoTimerLabel setText:[NSString stringWithFormat:@"00:0%d",currentVideoTime]];
    }
    currentVideoTime++;
}

- (IBAction)video:(id)sender {
    if (isPhotoMode) {
        isPhotoMode = NO;
        [self.videoModeButton setImage:[UIImage imageNamed:@"Video Selected.png"] forState:UIControlStateNormal];
        [self.cameraModeButton setImage:[UIImage imageNamed:@"Camera Unselected.png"] forState:UIControlStateNormal];
        [self.captureButton setImage:[UIImage imageNamed:@"Capture_video.png"] forState:UIControlStateNormal];
        self.imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    }
}

- (IBAction)photo:(id)sender {
    if (!isPhotoMode) {
        isPhotoMode = YES;
        self.videoTimerLabel.hidden = YES;
        [self.videoModeButton setImage:[UIImage imageNamed:@"Video Unselected.png"] forState:UIControlStateNormal];
        [self.cameraModeButton setImage:[UIImage imageNamed:@"Camera Selected.png"] forState:UIControlStateNormal];
        [self.captureButton setImage:[UIImage imageNamed:@"Capture.png"] forState:UIControlStateNormal];
        self.imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    }
}

- (IBAction)flashControl:(id)sender {
    if (!isFlashModeOn) {
        isFlashModeOn = YES;
        [self.imagePickerController setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
        [self.flashButton setImage:[UIImage imageNamed:@"Flash Selected.png"] forState:UIControlStateNormal];
    } else {
        isFlashModeOn = NO;
        [self.imagePickerController setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
        [self.flashButton setImage:[UIImage imageNamed:@"Flash-Unselected.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)changeCamera:(id)sender {
    if (self.imagePickerController.cameraDevice == UIImagePickerControllerCameraDeviceFront)
    {
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
    else
    {
        self.imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
}

- (IBAction)close:(id)sender {
    if (!isCapturingVideo) {
        [self.imagePickerController dismissViewControllerAnimated:YES completion:NULL];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)proceedAndAddScore:(id)sender {
    [self.captionTextView resignFirstResponder];
    [self.view bringSubviewToFront:self.stepThreeView];
    self.stepTwoFinishedImage.hidden = NO;
    [self.step3Label setTextColor:[UIColor orangeColor]];
    
}

- (IBAction)dismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButton:(id)sender {
    
    self.stepThreeFinishedImage.hidden = NO;
    switch (rating) {
        case 1:
            [self.ratingImageView setImage:[UIImage imageNamed:@"Crap-Preview.png"]];
            [self.ratingLabel setText:@"It's Crap"];
            [self.ratingLabel setTextColor:[UIColor colorWithRed:255.0/255.0 green:17.0/255.0 blue:1/255.0 alpha:1.0]];
            break;
        case 2:
            [self.ratingImageView setImage:[UIImage imageNamed:@"Boring-Preview.png"]];
            [self.ratingLabel setText:@"It's Boring"];
            [self.ratingLabel setTextColor:[UIColor colorWithRed:253.0/255.0 green:185.0/255.0 blue:9.0/255.0 alpha:1.0]];
            break;
        case 3:
            [self.ratingImageView setImage:[UIImage imageNamed:@"Better-Preview.png"]];
            [self.ratingLabel setText:@"It's Alright"];
            [self.ratingLabel setTextColor: [UIColor colorWithRed:46.0/255.0 green:106.0/255.0 blue:157.0/255.0 alpha:1.0]];
            break;
        case 4:
            [self.ratingImageView setImage:[UIImage imageNamed:@"Cracking-Preview.png"]];
            [self.ratingLabel setText:@"It's Cracking"];
            [self.ratingLabel setTextColor:[UIColor colorWithRed:85.0/255.0 green:197.0/255.0 blue:76.0/255.0 alpha:1.0]];
            break;
        default:
            break;
    }
    
    if (caption.length > 0) {
        [self.captionLabel setText:[NSString stringWithFormat:@"%@",caption]];
    }
    else
    {
        [self.captionLabel setText:@"You haven't given a caption!"];
    }
    self.ratingStrip.userInteractionEnabled = NO;
    [self.doneButton setEnabled:NO];
    [self.view bringSubviewToFront:self.capturedImage];
    [self.view bringSubviewToFront:self.ratingPreviewView];
    [self.view bringSubviewToFront:self.publishAndPrivacyBG];
    [self.view bringSubviewToFront:self.stepThreeFinishedImage];
}

- (IBAction)publish:(id)sender {
    [self showActivityIndicator];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    PFQuery *userLocation = [PFQuery queryWithClassName:@"UserLocation"];
    [userLocation whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
    [userLocation whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
    PFQuery *userLocationObjectsQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
    [userLocationObjectsQuery whereKey:@"userLocation" matchesQuery:userLocation];
    
    [userLocationObjectsQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            if (number == 6) {
                [[[UIAlertView alloc]initWithTitle:nil message:@"You already have posted 6 times today for this location" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            else
            {
                remainingPostsCount = 5 - number;
                NSDate *date = [userDefaults objectForKey:@"lastPublishedDate"];
                if (date != nil) {
                    NSTimeInterval timeIntervalSinceLastPost = [[NSDate date]timeIntervalSinceDate:date];
                    NSInteger minutes = timeIntervalSinceLastPost/60.0;
                    if (minutes >= 1) {
                        if (minutes > 1440) {
                            NSString *appDomain = [[NSBundle mainBundle]bundleIdentifier];
                            [userDefaults removePersistentDomainForName:appDomain];
                        }
                        [self allowPublish];
                    }
                    else
                    {
                        [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%ld min left for your next post",1 - (long)minutes] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                    }
                }
                else
                {
                    [self allowPublish];
                }

            }
        }
        else
        {
            [self showErrorAlertView:error.localizedDescription];
        }
    }];
}

- (void)allowPublish
{
    __block PFObject *userLocation;
    
    PFQuery *query = [PFQuery queryWithClassName:@"UserLocation"];
    [query whereKey:@"locationId" equalTo:[CCGlobalData sharedGlobalData].selectedPlace.identifier];
    [query whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            userLocation = object;
            [userLocation setObject:[NSNumber numberWithInteger:rating] forKey:@"score"];
            [userLocation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
             {
                 [self upload:userLocation];
             }];
        } else {
            userLocation = [PFObject objectWithClassName:@"UserLocation"];
            userLocation[@"locationId"] = [CCGlobalData sharedGlobalData].selectedPlace.identifier;
            userLocation[@"userObjectId"] = [PFUser currentUser];
            userLocation[@"score"]      = [NSNumber numberWithInteger:rating];
            PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:[CCGlobalData sharedGlobalData].selectedPlace.location.latitude longitude:[CCGlobalData sharedGlobalData].selectedPlace.location.longitude];
            userLocation[@"location"]   = geoPoint;
            [userLocation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
             {
                 [self upload:userLocation];
             }];
        }
    }];

}
- (void)upload:(PFObject *)userLocation {
    NSData *data;
    PFFile *dataFile;
    PFObject *userLocationObject = [PFObject objectWithClassName:@"UserLocationObjects"];
    if (videoURL != nil) {
        data = [NSData dataWithContentsOfURL:videoURL];
        dataFile = [PFFile fileWithName:@"video.mov" data:data];
        userLocationObject[@"isVideo"] = @YES;
        userLocationObject[@"videoThumbnail"] = [PFFile fileWithName:@"thumbnail.jpg" data:imageData];
    }
    else
    {
        dataFile = [PFFile fileWithName:@"image.jpg" data:imageData];
        userLocationObject[@"isVideo"] = @NO;
    }
    userLocationObject[@"dataFile"] = dataFile;
    userLocationObject[@"userLocation"] = userLocation;
    userLocationObject[@"score"] = [NSNumber numberWithInteger:rating];
    if (caption != nil) {
        userLocationObject[@"comment"] = caption;
    }
    userLocationObject[@"locationName"] = [CCGlobalData sharedGlobalData].selectedPlace.name;
    if ([CCGlobalData sharedGlobalData].selectedPlace.vicinity != nil) {
        userLocationObject[@"vicinity"]     = [CCGlobalData sharedGlobalData].selectedPlace.vicinity;
    }
    userLocationObject[@"isPrivate"] = [NSNumber numberWithBool:isPrivate];
    userLocationObject[@"user"] = [PFUser currentUser];
    [userLocationObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (!error) {
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:[NSDate date] forKey:@"lastPublishedDate"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             [self.postSuccessfulView.remainingPostsLabel setText:[NSString stringWithFormat:@"You can post %d more times",remainingPostsCount]];
             [self.view addSubview:self.postSuccessfulView];
             [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace = [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace + 1;
             [self performSelector:@selector(popToPubRatingVC) withObject:nil afterDelay:4.0];
         }
         else
         {
             [self showErrorAlertView:error.localizedDescription];
         }
         [self hideActivityIndicator];
     }];
}

- (void)popToPubRatingVC
{
    [self.postSuccessfulSubView removeFromSuperview];
    if ([[self backViewController]isKindOfClass:[CCHomeViewController class]]) {
        [self performSegueWithIdentifier:@"Rate_PubRating" sender:self];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}
- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (IBAction)setPrivate:(id)sender {
    isPrivate = YES;
    [self.publicButton setImage:[UIImage imageNamed:@"Globe_brown.png"] forState:UIControlStateNormal];
    [self.privateButton setImage:[UIImage imageNamed:@"Lock_White.png"] forState:UIControlStateNormal];
    [self.publicButton setBackgroundColor:[UIColor whiteColor]];
    [self.privateButton setBackgroundColor:[UIColor orangeColor]];
}

- (IBAction)setPublic:(id)sender {
    isPrivate = NO;
    [self.publicButton setImage:[UIImage imageNamed:@"Globe_White.png"] forState:UIControlStateNormal];
    [self.privateButton setImage:[UIImage imageNamed:@"Lock_Brown.png"] forState:UIControlStateNormal];
    [self.publicButton setBackgroundColor:[UIColor orangeColor]];
    [self.privateButton setBackgroundColor:[UIColor whiteColor]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch.view isDescendantOfView:self.ratingStrip]) {
        CGPoint touchLocation = [touch locationInView:self.ratingStrip];
        if (touchLocation.x >= 0.0 && touchLocation.x <= 120.0) {
            if (touchLocation.y >= 0.0 && touchLocation.y <= 130.0) {
                rating = 2;
                [self.ratingStrip setImage:[UIImage imageNamed:@"Boring-Full-View.png"]];
            }
            else
            {
                rating = 1;
                [self.ratingStrip setImage:[UIImage imageNamed:@"Crap-Full-View.png"]];
            }
        }
        else
        {
            if (touchLocation.y >= 0.0 && touchLocation.y <= 130.0) {
                rating = 3;
                [self.ratingStrip setImage:[UIImage imageNamed:@"Better-Full-View.png"]];
            }
            else
            {
                rating = 4;
                [self.ratingStrip setImage:[UIImage imageNamed:@"Cracking-Full-View.png"]];
            }

        }
       
    }
    [self.view endEditing:YES];
}

//- (void)setupSideMenuButton
//{
//    [self.topView removeFromSuperview];
//    self.navigationController.navigationBarHidden = NO;
//    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]]];
//    revealVC = [self revealViewController];
//    [revealVC tapGestureRecognizer];
//    UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-Friends"] style:UIBarButtonItemStylePlain target:revealVC action:@selector(revealToggle:)];
//    self.navigationItem.leftBarButtonItem = sideMenuButton;
//    self.navigationItem.hidesBackButton = NO;
//    
//    NSString *title = [NSString stringWithFormat:@"%@ (%@)",[CCGlobalData sharedGlobalData].selectedPlace.name, [CCGlobalData sharedGlobalData].selectedPlace.vicinity];
//    
//    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,250,40)];
//    navTitle.textAlignment = NSTextAlignmentLeft;
//    navTitle.text = title;
//    navTitle.font = [UIFont fontWithName:@"Helvetica" size:16.0];
//    navTitle.textColor = [UIColor whiteColor];
//    self.navigationItem.titleView = navTitle;
//}
@end
