//
//  CCFriendsTableViewCell.m
//  CrackingCam
//
//  Created by Mani Kishore on 6/23/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "CCFriendsTableViewCell.h"

@implementation CCFriendsTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.userProfilePic.layer.cornerRadius = 5.0;
    self.userProfilePic.clipsToBounds      = YES;
}

@end
