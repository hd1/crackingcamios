//
//  CCPubRatingViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/23/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCParentViewController.h"
#import "SWRevealViewController.h"

@interface CCPubRatingViewController : CCParentViewController<UICollectionViewDataSource,UICollectionViewDelegate,SWRevealViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *ratingImageView;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *timingsLabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *friendsCount;
@property (strong, nonatomic) IBOutlet UIButton *allButton;
@property (strong, nonatomic) IBOutlet UIButton *imagesButton;
@property (strong, nonatomic) IBOutlet UIButton *videosButton;
@property (strong, nonatomic) IBOutlet UIButton *addToQueueButton;
@property (strong, nonatomic) IBOutlet UILabel *noRatingLabel;

@property (nonatomic, strong) NSArray *friends;
- (IBAction)addToQueue:(id)sender;


@end
