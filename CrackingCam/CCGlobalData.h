//
//  CCGlobalData.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCGooglePlace.h"

@interface CCGlobalData : NSObject

+ (CCGlobalData *)sharedGlobalData;

@property (nonatomic, strong) NSString *deviceToken;

@property (nonatomic, strong) CCGooglePlace *selectedPlace;
@property (nonatomic, strong) NSString *currentDay;
@property (nonatomic, strong) NSMutableArray *fbFriendsIds;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic) NSInteger friendsCountAtSelectedPlace;
@property (nonatomic) NSInteger reviewsCountForSelectedPlace;
@property BOOL isSelectedPlaceInQueue;
@property (nonatomic, strong) NSString *fbFriendObjectId;
@property (nonatomic, strong) NSString *remoteNotificationLocationId;

@end
