//
//  CCMoreOptionsPopOver.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/13/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCMoreOptionsPopOver : UIView
@property (strong, nonatomic) IBOutlet UIView *popOverView;
@property (strong, nonatomic) IBOutlet UIView *buttonsContainerView;
@property (strong, nonatomic) IBOutlet UIButton *viewDetailsButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteFromQueueButton;

@end
