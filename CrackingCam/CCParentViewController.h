//
//  CCParentViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/2/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCParentViewController : UIViewController

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *loadingView;

- (void)showActivityIndicator;
- (void)hideActivityIndicator;
- (void)showErrorAlertView:(NSString *)error;

@end
