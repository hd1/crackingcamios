//
//  CCQueueViewController.m
//  CrackingCam
//
//  Created by Mani Kishore on 5/22/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>

#import "CCQueueViewController.h"
#import "CCGlobalData.h"
#import "CCQueueTableViewCell.h"
#import "CCGooglePlace.h"
#import "SWRevealViewController.h"
#import "CCPubRatingViewController.h"
#import "CCMoreOptionsPopOver.h"

@interface CCQueueViewController ()
{
    NSDateFormatter *dateFormatter;
    SWRevealViewController *revealVC;
    NSMutableArray *queue;
    NSMutableDictionary *reviewsCountDic;
    NSMutableDictionary *friendsCountDic;
    NSMutableDictionary *ratingsDic;
    int reviewsCount,friendCount,rating;
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) IBOutlet UILabel *emptyQueueMessageLabel;
@end

@implementation CCQueueViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    reviewsCountDic = [[NSMutableDictionary alloc]init];
    friendsCountDic = [[NSMutableDictionary alloc]init];
    ratingsDic = [[NSMutableDictionary alloc]init];
    queue = [[NSMutableArray alloc]init];
    
    [self setupSideMenuButton];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Splash-Screen.png"]];
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,290,40)];
    navTitle.textAlignment = NSTextAlignmentLeft;
    navTitle.text = @"Queues";
    navTitle.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    navTitle.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = navTitle;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBarBG.jpg"] forBarMetrics:UIBarMetricsDefault];
    
    locationManager = [[CLLocationManager alloc]init];
    [locationManager startUpdatingLocation];
    self.queueTableView.allowsMultipleSelectionDuringEditing = NO;
    [self.queueTableView setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Sticky-Footer-BG.png"]]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self showActivityIndicator];
    [queue removeAllObjects];
    PFQuery *query = [PFQuery queryWithClassName:@"UserQueue"];
    [query whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if(!error){
             for (PFObject *object in objects) {
                 CCGooglePlace *place = [[CCGooglePlace alloc]init];
                 NSDictionary *locationDetails = [object objectForKey:@"locationDetails"];
                 place.name = [locationDetails valueForKey:@"placeName"];
                 place.vicinity = [locationDetails valueForKey:@"vicinity"];
                 place.timings = [locationDetails objectForKey:@"timings"];
                 place.identifier = [object objectForKey:@"locationId"];
                 PFGeoPoint *location = [object objectForKey:@"location"];
                 place.location = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                 [queue addObject:place];
             }
             PFQuery *scoreQuery = [PFQuery queryWithClassName:@"UserLocation"];
             [scoreQuery selectKeys:[NSArray arrayWithObjects:@"userObjectId",@"score",@"locationId",nil]];
             [scoreQuery whereKey:@"locationId" containedIn:[queue valueForKey:@"identifier"]];
             [scoreQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
              if ([objects count] > 0) {
                 for (CCGooglePlace *queuedPlace in queue) {
                     int averageScore = 0;
                     int friendsCount = 0;
                     int relevantObjectsCount = 0;
                         for (PFObject *object in objects) {
                             if ([[object valueForKey:@"locationId"] isEqualToString:queuedPlace.identifier]) {
                                 for (PFObject *friend in [CCGlobalData sharedGlobalData].friends) {
                                     if ([[friend objectId] isEqualToString:[[object valueForKey:@"userObjectId"] objectId]]) {
                                         friendsCount++;
                                     }
                                 }
                                 relevantObjectsCount++;
                                 averageScore =  averageScore + [[object valueForKey:@"score"]intValue];
                             }
                         }
                         averageScore = ceilf(averageScore/relevantObjectsCount);
                        [friendsCountDic setValue:[NSNumber numberWithInt:friendsCount] forKey:queuedPlace.identifier];
                        [ratingsDic setValue:[NSNumber numberWithInt:averageScore] forKey:queuedPlace.identifier];
                    }
                 }
                 [self reloadQueueTableView];
             }];
         }
         else
         {
             [self showErrorAlertView:error.localizedDescription];
         }
         [self hideActivityIndicator];
     }];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)reloadQueueTableView
{
    if ([queue count] > 0) {
        [self.queueTableView reloadData];
        self.emptyQueueMessageLabel.hidden = YES;
        self.queueTableView.hidden = NO;
    }
    else
    {
        self.emptyQueueMessageLabel.hidden = NO;
        self.queueTableView.hidden = YES;
    }

}
- (void)setupSideMenuButton
{
    revealVC = [self revealViewController];
    [revealVC tapGestureRecognizer];
    
    UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-Friends.png"] style:UIBarButtonItemStylePlain target:revealVC action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = sideMenuButton;
}

#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [queue count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCQueueTableViewCell *cell;
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CCQueueTableViewCell" owner:self options:nil]objectAtIndex:0];
        [cell.getDirectionsButton addTarget:self action:@selector(getDirections:) forControlEvents:UIControlEventTouchUpInside];
    }
    CCGooglePlace *place = [queue objectAtIndex:indexPath.row];
    [cell.placeNameLabel setText:[NSString stringWithFormat:@"%@ (%@)",place.name, place.vicinity]];
    if ([place.timings objectForKey:[CCGlobalData sharedGlobalData].currentDay] != nil) {
        [cell.openingHoursLabel setText:[NSString stringWithFormat:@"%@",[place.timings objectForKey:[CCGlobalData sharedGlobalData].currentDay]]];
    }
    else
    {
        [cell.openingHoursLabel setText:[NSString stringWithFormat:@"Not Available"]];
    }
    cell.getDirectionsButton.tag = indexPath.row;
    
         switch ([[ratingsDic valueForKey:place.identifier]integerValue]) {
            case 0:
                 [cell.ratingImageView setImage:[UIImage imageNamed:@"No-Rating.png"]];
                 [cell.ratingLabel setText:@"No Rating"];
                 [cell.ratingLabel setTextColor:[UIColor colorWithRed:192.0/255.0 green:193.0/255.0 blue:192.0/255.0 alpha:1.0]];
                 break;
             case 1:
                 [cell.ratingImageView setImage:[UIImage imageNamed:@"Crap-Queue.png"]];
                 [cell.ratingLabel setText:@"It's Crap"];
                 [cell.ratingLabel setTextColor:[UIColor colorWithRed:255.0/255.0 green:17.0/255.0 blue:1/255.0 alpha:1.0]];
                 break;
             case 2:
                 [cell.ratingImageView setImage:[UIImage imageNamed:@"Boring-Queue.png"]];
                 [cell.ratingLabel setText:@"It's Boring"];
                 [cell.ratingLabel setTextColor:[UIColor colorWithRed:253.0/255.0 green:185.0/255.0 blue:9.0/255.0 alpha:1.0]];
                 break;
             case 3:
                 [cell.ratingImageView setImage:[UIImage imageNamed:@"Better-Queue.png"]];
                 [cell.ratingLabel setText:@"It's Alright"];
                 [cell.ratingLabel setTextColor: [UIColor colorWithRed:46.0/255.0 green:106.0/255.0 blue:157.0/255.0 alpha:1.0]];
                 break;
             case 4:
                 [cell.ratingImageView setImage:[UIImage imageNamed:@"Cracking-Queue.png"]];
                 [cell.ratingLabel setText:@"It's Cracking"];
                 [cell.ratingLabel setTextColor:[UIColor colorWithRed:85.0/255.0 green:197.0/255.0 blue:76.0/255.0 alpha:1.0]];
                 break;

             default:
                 break;
         }
         [cell.friendsCountLabel setText:[NSString stringWithFormat:@"%d Friends here",[[friendsCountDic valueForKey:place.identifier] integerValue]]];
         PFQuery *locationsQuery = [PFQuery queryWithClassName:@"UserLocation"];
         [locationsQuery whereKey:@"locationId" equalTo:place.identifier];
         PFQuery *mediaQuery = [PFQuery queryWithClassName:@"UserLocationObjects"];
         [mediaQuery whereKey:@"userLocation" matchesQuery:locationsQuery];
         [mediaQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error)
          {
              if(!error){
              [cell.reviewsCountLabel setText:[NSString stringWithFormat:@"%d Scores",number]];
              if ([reviewsCountDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] == nil) {
              [reviewsCountDic setValue:[NSNumber numberWithInt:number] forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
              }
              }
              else
              {
                  [self showErrorAlertView:error.localizedDescription];
              }
          }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCQueueTableViewCell *cell = (CCQueueTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UIView *selectedView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
    selectedView.backgroundColor = [UIColor orangeColor];
    cell.selectedBackgroundView = selectedView;

    [CCGlobalData sharedGlobalData].selectedPlace = [queue objectAtIndex:indexPath.row];
    [CCGlobalData sharedGlobalData].reviewsCountForSelectedPlace  = [[reviewsCountDic valueForKey:[NSString stringWithFormat:@"%d",indexPath.row]]integerValue];
    [CCGlobalData sharedGlobalData].friendsCountAtSelectedPlace = [[friendsCountDic valueForKey:[CCGlobalData sharedGlobalData].selectedPlace.identifier]integerValue];
    [CCGlobalData sharedGlobalData].isSelectedPlaceInQueue = YES;
    [self performSegueWithIdentifier:@"Queue_PubRating" sender:self];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self showActivityIndicator];
        CCGooglePlace *place = [queue objectAtIndex:indexPath.row];
        PFQuery *query = [PFQuery queryWithClassName:@"UserQueue"];
        [query whereKey:@"locationId" equalTo:place.identifier];
        [query whereKey:@"userObjectId" equalTo:[PFUser currentUser]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             [PFObject deleteAllInBackground:objects block:^(BOOL succeeded, NSError *error)
              {
                  if (succeeded) {
                      [queue removeObject:place];
                      [reviewsCountDic removeObjectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
                      [friendsCountDic removeObjectForKey:place.identifier];
                      [ratingsDic removeObjectForKey:place.identifier];
                      [self reloadQueueTableView];
                  }
                  [self hideActivityIndicator];
              }];
         }];
    }
}

- (void)getDirections:(UIButton *)sender
{
    CCGooglePlace *place = [queue objectAtIndex:sender.tag];
    NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@&near=%@",[place.name stringByReplacingOccurrencesOfString:@" " withString:@"+"],[place.vicinity stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

@end
