//
//  CCFriendsTableViewCell.h
//  CrackingCam
//
//  Created by Mani Kishore on 6/23/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCFriendsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailsTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePic;

@end
