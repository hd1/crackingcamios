//
//  CCGalleryViewController.h
//  CrackingCam
//
//  Created by Mani Kishore on 5/26/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCGalleryViewController : UIViewController

@property (nonatomic, strong) NSArray *media;
@property (strong, nonatomic) IBOutlet UICollectionView *galleryCollectionView;
@property BOOL isFromFriendsActivity;
@property (nonatomic, strong) NSIndexPath *indexPath;

- (IBAction)dismissGallery:(id)sender;

@end
